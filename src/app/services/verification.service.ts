import { Injectable } from '@angular/core';
import { RegistrationInfo } from '../model/registration-info';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SessionService } from './session.service';

@Injectable({providedIn : "root"})
export class VerificationService {

    private registrationInfo: RegistrationInfo = null;
    private httpHeaders = new HttpHeaders({ 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1YjY5MzYzYTc4NGI3ZjRmZjNiNTI2OGQiLCJzdWIiOiJBdXhpbG8iLCJpc3MiOiJrY1FtUnkzMUNOMGhBaXU5bHFYQmRqdHNaaHlxN0ZPVCJ9.QbZmNqIMRDdWyW32Bi7y1t6P1r9KGx95kPhGWzQUSoo',
     'Content-Type' : 'application/json' });
    public onLogin: Subject<void> = new Subject<void>();

    constructor(
        private httpClient: HttpClient,
        private sessionService: SessionService
    ) {}

    clearRegistrationInfo() {
        this.registrationInfo = new RegistrationInfo();
    }

    getRegistrationInfo() {
        return this.registrationInfo;
    }

    setMobileOtp(otp: string) {
        this.registrationInfo.mobileOtp = otp;
    }   

    setEmailOtp(otp: string) {
        this.registrationInfo.emailOtp = otp;
    }

    getMobileOtp(mobileNumber: string): Observable<any> {
        const body = {
            functionInstanceName : "send_sms_auxilo",
            id : mobileNumber
        }

        this.registrationInfo = new RegistrationInfo();
        this.registrationInfo.mobile = mobileNumber;
        return this.httpClient.post(environment.baseUrl + environment.smsOtpUrl, 
                body,
                { 
                headers: this.httpHeaders,
                observe: 'body'
            })
    }

    getEmailOtp(emailId: string): Observable<any> {
        this.registrationInfo.email = emailId;
        
        const body =  {sender: "Auto", 
                        id: emailId, 
                        template: "OTP_EMAIL", 
                        functionInstanceName: "generateAndSendEmailOtp_Auxilo", 
                        payload: {
                            email: emailId, 
                            language: "ENG",
                        }
                    }
        
        return this.httpClient.post(environment.baseUrl + environment.emailOtpUrl, 
                body,
                { observe: 'body',
                headers: this.httpHeaders})
    }

    verifyOtp(): Observable<any> {
        const body = {
            email: this.registrationInfo.email,
            emailOtp: this.registrationInfo.emailOtp,
            mobile: this.registrationInfo.mobile,
            mobileOtp: this.registrationInfo.mobileOtp
        }
        return this.httpClient.post("https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/authenticate",
            body,
            {
                headers: { 'Content-Type': 'application/json', },
                observe: 'body'
            }).pipe(map((response: any) => {
                this.sessionService.setAuthToken(response.body.token);
                this.onLogin.next();
                return response;
            }))
    }

    logout(){
        if(localStorage.getItem('Authorization')){
            localStorage.clear();
        }
    }

}