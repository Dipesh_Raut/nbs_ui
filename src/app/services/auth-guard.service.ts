import { CanActivate, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { SessionService } from './session.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private sessionService: SessionService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.sessionService.getAuthToken()) {
            return true;
        }

        this.router.navigate(['/'], { queryParams: { returnUrl: state.url} });
        return false;
    }
}

@Injectable()
export class AntiAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private sessionService: SessionService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const authToken = this.sessionService.getAuthToken();

        if (this.sessionService.getAuthToken()) {
            // this.router.navigate(['/loan-purpose']);
            return false;
        }

        return true;
    }
}