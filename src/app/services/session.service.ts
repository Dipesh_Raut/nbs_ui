import { Injectable } from '@angular/core';

@Injectable({providedIn : "root"})
export class SessionService {
    constructor() { }

    clearSession() {
        localStorage.clear();
    }

    clearKeys(...keys: any[]) {
        if (keys) {
            for (const key of keys) {
                localStorage.removeItem(key);
            }
        }
    }

    setAuthToken(authToken: string) {
        if (authToken && authToken.trim().length > 0) {
            localStorage.setItem('Authorization', 'Bearer ' + authToken);
        }
    }

    setPerfiosPayload(payload: string) {
        console.log("===============");
        console.log(payload);
        localStorage.setItem('perfiosPayload', payload);
    }

    setPerfiosSignature(signature: string) {
        localStorage.setItem('perfiosSignature', signature);
    }

    setApplicantType(type: string) {
        localStorage.setItem('applicantType', type);
    }

    getApplicantType() {
        const applicantType = localStorage.getItem('applicantType');
        return applicantType && applicantType.trim().length > 0 ? applicantType : null;
    }

    getPerfiosPayload() {
        const payload = localStorage.getItem('perfiosPayload');
        return payload && payload.trim().length > 0 ? payload : null;
    }

    getPerfiosSignature() {
        const signature = localStorage.getItem('perfiosSignature');
        return signature && signature.trim().length > 0 ? signature : null;
    }
   
    getAuthToken() {
        const authToken = localStorage.getItem('Authorization');
        return authToken && authToken.trim().length > 0 ? authToken : null;
    }

    resetAuthToken() {
        this.clearKeys('Authorization');
    }
}