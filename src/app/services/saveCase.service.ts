import { Injectable } from '@angular/core';
import { RegistrationInfo } from '../model/registration-info';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Subject, Subscription } from 'rxjs';
import { BasicDetails } from '../model/basicDetails.model';
import { Case } from '../model/case.model';
import { map } from 'rxjs/operators';
import { StudentDetails } from '../student-details/shared/student-details.model';
import { Course } from '../course/shared/course.model';
import { Parent } from '../parent/shared/parent.model';
import { CoApplicant } from '../co-applicant/shared/co-applicant.model';
import { SessionService } from './session.service';

@Injectable({ providedIn: "root" })
export class SaveCaseService {

   private static readonly CASE_STORAGE_KEY = 'Case'
   private case: Case = null;

   subjectCase = new Subject<Case>();
   subjectBasicDetails = new Subject<BasicDetails>();
   subjectStudentDetails = new Subject<StudentDetails>();
   subjectCoApplicantDetails = new Subject<CoApplicant>();
   subjectParentDetails = new Subject<Parent>();
   subjectCourseDetails = new Subject<Course>();
   subjectSaveCase = new Subject<Case>();

   constructor(
      private httpClient: HttpClient,
      private sessionService: SessionService
   ) { }


   fetchCase() {
      const httpHeaders = new HttpHeaders({ 'Authorization': this.sessionService.getAuthToken(), 'Content-Type': 'application/json' });

      return this.httpClient.get<Case>(
         environment.caseUrl,
         {
            observe: 'body',
            headers: httpHeaders,
            reportProgress: true
         }
      ).pipe(map((response: any) => {
         if (response && response.success && response.savedCase) {
            this.case = response.savedCase;
         }
         return response;
      }));

   }

   saveCase() {
      const httpHeaders = new HttpHeaders({ 'Authorization': this.sessionService.getAuthToken(), 'Content-Type': 'application/json' });
      this.case.functionInstanceName = "saveCase";
      return this.httpClient.post<Case>(
         environment.caseUrl,
         this.case,
         {
            observe: 'body',
            headers: httpHeaders,
            reportProgress: true,
            //withCredentials: true
         }
      ).pipe(map((response: any) => {
         if (response && response.success && response.savedCase) {
            this.case = response.savedCase;
            this.subjectSaveCase.next(this.case);
         }

         return response;
      }));

   }

   getCase(): void {
      if (this.case && this.case != undefined && this.case != null) {
         this.subjectCase.next(this.case);
      } else {
         this.fetchCase().subscribe(
            response => {
               if (!this.case || this.case == undefined || this.case == null) {
                  this.case = new Case();
               }
               
               this.subjectCase.next(this.case);
            },
            error => {
               this.subjectCase.error(error);
            }
         );
      }
   }

   getBasicDetails() {
      if (this.case && this.case != undefined && this.case != null) {
         this.subjectBasicDetails.next(this.case.basicDetails);
      } else {
         this.fetchCase().subscribe(
            response => {
               if (!this.case || this.case == undefined || this.case == null) {
                  this.case = new Case();
               }
               
               this.subjectBasicDetails.next(this.case.basicDetails);
            },
            error => {
               this.subjectBasicDetails.error(error);
            }
         );
      }
   }

   getStudentDetails() {
      if (this.case && this.case != undefined && this.case != null) {
         this.subjectStudentDetails.next(this.case.studentDetails);
      } else {
         this.fetchCase().subscribe(
            response => {
               if (!this.case || this.case == undefined || this.case == null) {
                  this.case = new Case();
               }
               
               this.subjectStudentDetails.next(this.case.studentDetails);
            },
            error => {
               this.subjectStudentDetails.error(error);
            }
         );
      }
   }

   getCoApplicantDetails() {
      if (this.case && this.case != undefined && this.case != null) {
         this.subjectCoApplicantDetails.next(this.case.coApplicant);
      } else {
         this.fetchCase().subscribe(
            response => {
               if (!this.case || this.case == undefined || this.case == null) {
                  this.case = new Case();
               }
               
               this.subjectCoApplicantDetails.next(this.case.coApplicant);
            },
            error => {
               this.subjectCoApplicantDetails.error(error);
            }
         );
      }
   }

   getParentDetails() {
      if (this.case && this.case != undefined && this.case != null) {
         this.subjectParentDetails.next(this.case.parent);
      } else {
         this.fetchCase().subscribe(
            response => {
               if (!this.case || this.case == undefined || this.case == null) {
                  this.case = new Case();
               }
               
               this.subjectParentDetails.next(this.case.parent);
            },
            error => {
               this.subjectParentDetails.error(error);
            }
         );
      }
   }

   getCourseDetails() {
      if (this.case && this.case != undefined && this.case != null) {
         this.subjectCourseDetails.next(this.case.course);
      } else {
         this.fetchCase().subscribe(
            response => {
               if (!this.case || this.case == undefined || this.case == null) {
                  this.case = new Case();
               }
               
               this.subjectCourseDetails.next(this.case.course);
            },
            error => {
               this.subjectCourseDetails.error(error);
            }
         );
      }
   }

   setBasicDetails(basicDetails: BasicDetails, skipApiCall?: boolean) {
      if (basicDetails) {
         const basicDetailsSubscription: Subscription = this.subjectCase.subscribe(
            next => {
               this.case.basicDetails = basicDetails;
               if (!skipApiCall) {
                  this.saveCase().subscribe(
                     response => {
                        console.log(response);
                     },
                     error => {
                        console.log(error);
                     }
                  );
               }
               basicDetailsSubscription.unsubscribe();
            },
            error => {
               this.subjectSaveCase.error('Failed to fetch case object');
               basicDetailsSubscription.unsubscribe();
            }
         );
         this.getCase();
      }
   }

   setStudentDetails(studentDetails: StudentDetails, skipApiCall?: boolean) {
      if (studentDetails) {
         const studentDetailsSubscription: Subscription = this.subjectCase.subscribe(
            next => {
               this.case.studentDetails = studentDetails;
               if (!skipApiCall) {
                  this.saveCase().subscribe(
                     response => {
                        console.log(response);
                     },
                     error => {
                        console.log(error);
                     }
                  );
               }
               studentDetailsSubscription.unsubscribe();
            },
            error => {
               this.subjectSaveCase.error('Failed to fetch case object');
               studentDetailsSubscription.unsubscribe();
            }
         );
         this.getCase();
      }
   }

   setcoApplicantDetails(coApplicant: CoApplicant, skipApiCall?: boolean) {
      if (coApplicant) {
         const coApplicantSubscription: Subscription = this.subjectCase.subscribe(
            next => {
               this.case.coApplicant = coApplicant;
               if (!skipApiCall) {
                  this.saveCase().subscribe(
                     response => {
                        console.log(response);
                     },
                     error => {
                        console.log(error);
                     }
                  );
               }
               coApplicantSubscription.unsubscribe();
            },
            error => {
               this.subjectSaveCase.error('Failed to fetch case object');
               coApplicantSubscription.unsubscribe();
            }
         );
         this.getCase();
      }
   }

   setParentDetails(parent: Parent, skipApiCall?: boolean) {
      if (parent) {
         const parentSubscription: Subscription = this.subjectCase.subscribe(
            next => {
               this.case.parent = parent;
               if (!skipApiCall) {
                  this.saveCase().subscribe(
                     response => {
                        console.log(response);
                     },
                     error => {
                        console.log(error);
                     }
                  );
               }
               parentSubscription.unsubscribe();
            },
            error => {
               this.subjectSaveCase.error('Failed to fetch case object');
               parentSubscription.unsubscribe();
            }
         );
         this.getCase();
      }
   }

   setCourseDetails(course: Course, skipApiCall?: boolean) {
      if (course) {
         const courseSubscription: Subscription = this.subjectCase.subscribe(
            next => {
               this.case.course = course;
               if (!skipApiCall) {
                  this.saveCase().subscribe(
                     response => {
                        console.log(response);
                     },
                     error => {
                        console.log(error);
                     }
                  );
               }
               courseSubscription.unsubscribe();
            },
            error => {
               this.subjectSaveCase.error('Failed to fetch case object');
               courseSubscription.unsubscribe();
            }
         );
         this.getCase();
      }
   }
}