import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({ providedIn: "root" })
export class LookupService {
    constructor(
        private httpClient: HttpClient) { }

getLookupDetails(lookupType?: string, lookupKey?:string){
    const httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
    const body ={
    "lookupType" : lookupType,
	"lookupKey" : lookupKey
    }
    return this.httpClient.post<any>(
      environment.lookupUrl,
      body,
      {
         observe: 'body',
         headers: httpHeaders,
         reportProgress: true
      }
   ).pipe(map((response: any) => {
      if (response && response.success && response.lookups) {
      }
      return response.lookups;
   }));
  
  }
}