import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { environment } from 'src/environments/environment';


@Injectable()
export class PerfiosService {


    private case = null;
    constructor(private httpClient: HttpClient) {
      
    }

    fetchITR(transactionId?: string) {
      	const httpHeaders = new HttpHeaders({ 'Authorization': localStorage.getItem('Authorization'),'Content-Type' : 'application/json' });
		const body = {
			'functionInstanceName':"auxilo_perfios_fetchITR",
			'perfiosTransactionId':transactionId
		}
        return this.httpClient.post<any>(
           environment.fetchitr,
            body,
            {
              observe: 'body',
              headers: httpHeaders,
              reportProgress: true,
              //withCredentials: true
            }
          )
    }

}
