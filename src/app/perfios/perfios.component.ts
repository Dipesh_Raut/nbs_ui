declare var closeModal: any;
declare var showModal: any;
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HeaderService } from '../header.service';
import { PerfiosService } from './shared/perfios.service';
import { BasicDetails } from '../model/basicDetails.model';
import { Parent } from '../parent/shared/parent.model';
import { SaveCaseService } from '../services/saveCase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { StudentDetails } from '../student-details/shared/student-details.model';
import { CoApplicant } from '../co-applicant/shared/co-applicant.model';
import { Subscription } from 'rxjs';
import { SessionService } from '../services/session.service';
//import { Subscription } from 'rxjs/Subscription';



@Component({
    selector: 'app-perfios',
    templateUrl: './perfios.component.html',
    styleUrls: ['./perfios.component.css']
})
export class PerfiosComponent implements OnInit, OnDestroy {
    @Input() applicant: string;
    perfiosTransactionCompleted: string;
    perfiosTransactionFailure: string;
    perfiosTransactionTimeout: string;
    perfiosTransactionCancelled: string;
    perfiosTransactionError: string;
    attributes = {};
    studentDetails: StudentDetails;
    coApplicantDetails: CoApplicant;
    message: string;
    buttonMessage: string;
    txnId: string;

    studentDetailsSubscription: Subscription;
    coAppDetailsSubscription: Subscription;
    saveCaseSubscription: Subscription;

    //private subscription: Subscription;
    constructor(
        private route: ActivatedRoute,
        private perfiosService: PerfiosService,
        private saveCaseService: SaveCaseService,
        private sessionService: SessionService
    ) {
        this.studentDetails = new StudentDetails();
        this.perfiosTransactionCompleted = 'COMPLETED';
        this.perfiosTransactionCancelled = 'CANCELLED';
        this.perfiosTransactionError = 'ERROR';
        this.perfiosTransactionFailure = 'REPORT_DELIVERY_FAILED';
        this.perfiosTransactionTimeout = 'TIMEOUT';
        this.message = '';
        this.buttonMessage = '';
        this.studentDetails = new StudentDetails();
        this.coApplicantDetails = new CoApplicant();
    }
    ngOnInit() {
        if(this.sessionService.getApplicantType() === "student"){
            this.subscribeToStudentDetails();
        }
        if(this.sessionService.getApplicantType() === "coapplicant"){
            this.subscribeToCoApplicantDetails();
        }
        this.route.params.subscribe(params => {
            this.attributes = this.route.snapshot.queryParams;
            console.log(this.attributes);
        });
        this.saveCaseService.getStudentDetails();
        this.saveCaseService.getCoApplicantDetails();
        this.subscribeToSaveCase();
    }

    ngOnDestroy() {
        if (this.studentDetailsSubscription && !this.studentDetailsSubscription.closed) {
            this.studentDetailsSubscription.unsubscribe();
        }
        if (this.coAppDetailsSubscription && !this.coAppDetailsSubscription.closed) {
            this.coAppDetailsSubscription.unsubscribe();
        }
    }

    subscribeToSaveCase() {
        this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
          next => {
          },
          error => {
          }
        );
      }
    

    subscribeToStudentDetails() {
        this.studentDetailsSubscription = this.saveCaseService.subjectStudentDetails.subscribe(
            next => {
                this.studentDetails = next;
                if (!this.studentDetails.itrReceived)
                {
                    if (this.attributes['status'] !== this.perfiosTransactionCompleted) {
                        this.message = 'Something went wrong please try again';
                        this.buttonMessage = 'Try Again';
                    }
                    if (this.attributes['status'] === this.perfiosTransactionCompleted) {
                        this.perfiosService.fetchITR(this.attributes['txnId']).subscribe(response => {
                            this.message = 'Your ITR was fetched successfully please click on the button to continue your application';
                            this.buttonMessage = 'Continue Application';
                            this.studentDetails.itrReceived = true;
                            this.saveCaseService.setStudentDetails(this.studentDetails);
                        });
                    }
                }
            },
            error => {
                console.error('Error while fetching student details', error);
            }
        );
    }

    subscribeToCoApplicantDetails() {
        this.coAppDetailsSubscription = this.saveCaseService.subjectCoApplicantDetails.subscribe(
            next => {
                this.coApplicantDetails = next;
                if (!this.coApplicantDetails.itrReceived)
                {
                    if (this.attributes['status'] !== this.perfiosTransactionCompleted) {
                        this.message = 'Something went wrong please try again';
                        this.buttonMessage = 'Try Again';
                    }
                    if (this.attributes['status'] === this.perfiosTransactionCompleted) {
                        this.perfiosService.fetchITR(this.attributes['txnId']).subscribe(response => {
                            this.message = 'Your ITR was fetched successfully please click on the button to continue your application';
                            this.buttonMessage = 'Continue Application';
                            this.coApplicantDetails.itrReceived = true;
                            this.saveCaseService.setcoApplicantDetails(this.coApplicantDetails);
                        });
                    }
                }
            },
            error => {
                console.error('Error while fetching student details', error);
            }
        );
    }

    proceed() {
        window.close();
    }
}
