import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng5SliderModule } from 'ng5-slider';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { MobileRegistrationComponent } from './mobile-registration/mobile-registration.component';
import { VerificationComponent } from './verification/verification.component';
import { EmailComponent } from './email/email.component';
import { PancardComponent } from './pancard/pancard.component';
import { LoanPurposeComponent } from './loan-purpose/loan-purpose.component';
import { HttpClientModule } from '@angular/common/http';
import { NewloanComponent } from './newloan/newloan.component';
import { StudentDetailsComponent } from './student-details/student-details.component';
import { CoApplicantComponent } from './co-applicant/co-applicant.component';
import { ParentComponent } from './parent/parent.component';
import { CourseComponent } from './course/course.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OneStepAwayComponent } from './one-step-away/one-step-away.component';
import { ScanComponent } from './scan/scan.component';
import { ScanPassportComponent } from './scan-passport/scan-passport.component';
import { ScanPancardComponent } from './scan-pancard/scan-pancard.component';
import { ScanAadhaarcardComponent } from './scan-aadhaarcard/scan-aadhaarcard.component';
import { CoApplicantNfComponent } from './co-applicant-nf/co-applicant-nf.component';
import { VerifyPassportComponent } from './verify-passport/verify-passport.component';
import { VerifyPancardComponent } from './verify-pancard/verify-pancard.component';
import { VerifyAadhaarcardComponent } from './verify-aadhaarcard/verify-aadhaarcard.component';
import { SummaryComponent } from './summary/summary.component';
import { PerfiosComponent } from './perfios/perfios.component';
import { StartPerfiosComponent } from './startperfios/startperfios.component';
import { PerfiosBankComponent } from './perfiosbank/perfiosbank.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { DocumentUploadComponent } from './document-upload/document-upload.component';
import { OneStepAwayService } from './one-step-away/shared/one-step-away.service';
import { SaveCaseService } from './services/saveCase.service';
import { AuthGuard, AntiAuthGuard } from './services/auth-guard.service';
import { CommonModule } from '@angular/common';
import {ToastrModule} from 'ngx-toastr';
import { StudentDetailsService } from './student-details/shared/student-details.service';
import { CoApplicantService } from './co-applicant/shared/co-applicant.service';
import { PerfiosService } from './perfios/shared/perfios.service';
import { TandcComponent } from './tandc/tandc.component';
import { NgxUiLoaderModule, NgxUiLoaderRouterModule } from  'ngx-ui-loader';
import { NgxCurrencyModule } from "ngx-currency";
import { SignUpComponent } from './sign-up/sign-up.component';
import { CommonVerificationComponent } from './common-verification/common-verification.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GetstartedComponent,
    MobileRegistrationComponent,
    VerificationComponent,
    EmailComponent,
    PancardComponent,
    LoanPurposeComponent,
    NewloanComponent,
    StudentDetailsComponent,
    CoApplicantComponent,
    ParentComponent,
    CourseComponent,
    OneStepAwayComponent,
    ScanComponent,
    ScanPassportComponent,
    ScanPancardComponent,
    ScanAadhaarcardComponent,
    CoApplicantNfComponent,
    VerifyPassportComponent,
    VerifyPancardComponent,
    VerifyAadhaarcardComponent,
    SummaryComponent,
    ThankyouComponent,
    DocumentUploadComponent,
    PerfiosBankComponent,
    PerfiosComponent,
    TandcComponent,
    StartPerfiosComponent,
    SignUpComponent,
    CommonVerificationComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    //SweetAlert2Module.forRoot(),
    Ng5SliderModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    NgxUiLoaderModule, // import NgxUiLoaderModule
    NgxUiLoaderRouterModule,
    NgxCurrencyModule,
    ],
  providers: [OneStepAwayService,SaveCaseService,StudentDetailsService,PerfiosService,
    CoApplicantService, AuthGuard, AntiAuthGuard],
  bootstrap: [AppComponent]
})


export class AppModule {
  

  
 }


