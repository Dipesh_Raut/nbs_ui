import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { MobileRegistration } from './mobile-registration.model';

@Injectable()
export class MobileRegistrationService {

	constructor(private http: Http) { }

	getList(): Observable<MobileRegistration[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as MobileRegistration[]));
	}
	postOrder(cart): Observable<MobileRegistration[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as MobileRegistration[]));
	}
}