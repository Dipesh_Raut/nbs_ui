import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VerificationService } from '../services/verification.service';
import { HeaderService } from '../header.service';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-mobile-registration',
  templateUrl: './mobile-registration.component.html',
  styleUrls: ['./mobile-registration.component.css']
})
export class MobileRegistrationComponent implements OnInit {
  country = '';
  success = '';
  countryflag = '';
  successicon = '';
  mobileNumber: string = '';
  title: string = 'Registration';

  constructor(
    private router: Router,
    private verificationService: VerificationService,
    private headerService: HeaderService,
    private route: ActivatedRoute,
    private sessionService: SessionService
  ) { }

  public processMobile() {
    // return this.httpClient.get('url');
    this.countryflag = 'assets/images/india-flag.png';
    this.country = 'India';
    this.success = 'success'; // danger for error
    this.successicon = 'fa-check-circle'; // fa-times-circle
  }
  public gotoOTP(){
    this.verificationService.getMobileOtp(this.mobileNumber).subscribe((response) => {
      if(response.success) {     
        this.router.navigateByUrl('verification/mobile');
      } else {
        // TODO: error handeling
      }
    });
  }
  ngOnInit() {
    this.sessionService.resetAuthToken();

    this.countryflag = 'assets/images/india-flag.png';
    this.headerService.setTitle('');
    console.log(this.route);
    if(this.route.snapshot.routeConfig.path === 'mobile-login'){
      this.title = "Login";
    }
  }

  _keyPress(event: any) {
    if(this.mobileNumber.length >= 9)
    {
      this.processMobile()
    }
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    
}

}
