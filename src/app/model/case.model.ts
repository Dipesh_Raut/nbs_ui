import { StudentDetails } from 'src/app/student-details/shared/student-details.model';
import { CoApplicant } from '../co-applicant/shared/co-applicant.model';
import { Parent } from '../parent/shared/parent.model';
import { Course } from '../course/shared/course.model';
import { BasicDetails } from './basicDetails.model';

export class Case{
    userId: string;
    functionInstanceName: string;
    basicDetails: BasicDetails = new BasicDetails();
    studentDetails: StudentDetails = new StudentDetails();
    coApplicant: CoApplicant = new CoApplicant();
    parent: Parent = new Parent();
    course: Course = new Course();
}