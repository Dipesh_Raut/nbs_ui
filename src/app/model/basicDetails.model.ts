export class BasicDetails{
    mobile: string;
    email: string;
    pan: string;
    serviceLocation: string;
	educationCountry: string;
}