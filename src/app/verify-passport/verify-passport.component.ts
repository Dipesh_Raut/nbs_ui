import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-verify-passport',
  templateUrl: './verify-passport.component.html',
  styleUrls: ['./verify-passport.component.css']
})
export class VerifyPassportComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Verify Passport');
  }

}
