import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-tandc',
  templateUrl: './tandc.component.html',
  styleUrls: ['./tandc.component.css']
})
export class TandcComponent implements OnInit {

  constructor( private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Terms and Conditions');
  }

}
