import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Verification } from './verification.model';

@Injectable()
export class VerificationService {

	constructor(private http: Http) { }

	getList(): Observable<Verification[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Verification[]));
	}
	postOrder(cart): Observable<Verification[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Verification[]));
	}
}