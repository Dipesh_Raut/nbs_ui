export class Verification {
	mediumInfo: string = "";
	otpSource: any;
	isInvalidState: boolean = false;
	otp: number;
	otpDigit1: string;
	otpDigit2: string;
	otpDigit3: string;
	otpDigit4: string;
	onfirstTextBox: boolean = true;
}