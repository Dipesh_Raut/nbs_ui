import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OtpSource } from '../model/otp-source.enum';
import { VerificationService } from '../services/verification.service';
import { HeaderService } from '../header.service';
import { SaveCaseService } from '../services/saveCase.service';
import { Subscription } from 'rxjs';
import { BasicDetails } from '../model/basicDetails.model';
import { RegistrationInfo } from '../model/registration-info';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private verificationService: VerificationService,
    private headerService: HeaderService,
    private saveCaseService: SaveCaseService) { }


  mediumInfo: string = "";
  otpSource: OtpSource;
  isInvalidState: boolean = false;
  otp: number;
  otpDigit1: string;
  otpDigit2: string;
  otpDigit3: string;
  otpDigit4: string;
  onfirstTextBox: boolean = true;
  

  basicDetails = new BasicDetails();
  registrationInfo: RegistrationInfo;
  basicDetailsSubscription: Subscription;
  saveCaseSubscription: Subscription;

  @ViewChild('setFocus') private elementRef: ElementRef;

  public ngAfterViewInit(): void {
    this.elementRef.nativeElement.focus();
  }

  ngOnInit() {
    this.headerService.setTitle('');
    this.subscribeToBasicDetails();
    this.subscribeToSaveCase();

    this.route.params.subscribe(() => {
      this.otpSource = OtpSource[this.route.snapshot.params.otpSource.toUpperCase()] as OtpSource;
      const regInfo = this.verificationService.getRegistrationInfo();
      if (this.otpSource == OtpSource.EMAIL) {
        this.mediumInfo = regInfo.email;
      } else if (this.otpSource == OtpSource.MOBILE) {
        this.mediumInfo = regInfo.mobile;
      } else {
        this.isInvalidState = false;
      }
      this.onfirstTextBox = true;

    })
  }

  ngOnDestroy() {
    if (this.basicDetailsSubscription && !this.basicDetailsSubscription.closed) {
      this.basicDetailsSubscription.unsubscribe();
    }
    if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
      this.saveCaseSubscription.unsubscribe();
    }
  }

  subscribeToBasicDetails() {
    this.basicDetailsSubscription = this.saveCaseService.subjectBasicDetails.subscribe(
      next => {
        this.basicDetails = next;
        this.basicDetails.mobile = this.registrationInfo.mobile;
        this.basicDetails.email = this.registrationInfo.email;
        this.saveCaseService.setBasicDetails(this.basicDetails);
      },
      error => {
        console.error('Error while fetching basic details', error);
      }
    );
  }

  subscribeToSaveCase() {
    this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
      next => {
        this.router.navigateByUrl('/pancard');
      },
      error => {

      }
    );
  }

  setOtp() {
    if (this.otpSource == OtpSource.EMAIL) {
      this.verificationService.setEmailOtp(this.otpDigit1 + this.otpDigit2 + this.otpDigit3 + this.otpDigit4);
      this.verificationService.verifyOtp().subscribe(response => {
        if (response.statusCode == 200) {
          // this.verificationService
          //TO DO: add auth_token
          this.registrationInfo = this.verificationService.getRegistrationInfo();

          this.saveCaseService.getBasicDetails();
          // Check in subscribeToBasicDetails
        }
        else {
          alert(response.body.error);
          this.router.navigateByUrl('/');
        }
      })
    } else if (this.otpSource == OtpSource.MOBILE) {
      this.verificationService.setMobileOtp(this.otpDigit1 + this.otpDigit2 + this.otpDigit3 + this.otpDigit4);
      this.router.navigateByUrl('/email')
    }
  }
  setAutoFocusTo(nextFieldID,event:any,thisFeild) {
    //  this.onfirstTextBox= false;
    if(event.key == "Backspace")
    {
      console.log(thisFeild)
      if(thisFeild=='second')
      {
        document.getElementById('first').focus();
      }
      else if (thisFeild=='third')
      {
        document.getElementById('second').focus();
      }
      else if (thisFeild=='fourth')
      {
        document.getElementById('third').focus();
      }
      
    }
    else if (nextFieldID) {
      document.getElementById(nextFieldID).focus();
    }
  }
}
