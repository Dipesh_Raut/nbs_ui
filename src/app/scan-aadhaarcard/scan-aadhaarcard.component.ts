import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-scan-aadhaarcard',
  templateUrl: './scan-aadhaarcard.component.html',
  styleUrls: ['./scan-aadhaarcard.component.css']
})
export class ScanAadhaarcardComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Scan Aadhaarcard');
  }
}
