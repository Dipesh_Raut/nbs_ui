import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanAadhaarcardComponent } from './scan-aadhaarcard.component';

describe('ScanAadhaarcardComponent', () => {
  let component: ScanAadhaarcardComponent;
  let fixture: ComponentFixture<ScanAadhaarcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanAadhaarcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanAadhaarcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
