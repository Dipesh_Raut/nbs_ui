import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.css']
})
export class DocumentUploadComponent implements OnInit {

  constructor(private headerService: HeaderService, private _location: Location) { }

  modal: any;

  ngOnInit() {
    this.headerService.setTitle('Document Upload');

  }
  backClicked() {
    this._location.back();
  }
}
