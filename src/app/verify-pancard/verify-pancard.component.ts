import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-verify-pancard',
  templateUrl: './verify-pancard.component.html',
  styleUrls: ['./verify-pancard.component.css']
})
export class VerifyPancardComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Verify Pancard');
  }

}
