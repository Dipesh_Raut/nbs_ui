import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyPancardComponent } from './verify-pancard.component';

describe('VerifyPancardComponent', () => {
  let component: VerifyPancardComponent;
  let fixture: ComponentFixture<VerifyPancardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyPancardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyPancardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
