declare var closeModal: any;
declare var showModal: any;
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HeaderService } from '../header.service';
import { BasicDetails } from '../model/basicDetails.model';
import { Parent } from '../parent/shared/parent.model';
import { SaveCaseService } from '../services/saveCase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { StudentDetails } from '../student-details/shared/student-details.model';
import { CoApplicant } from '../co-applicant/shared/co-applicant.model';
import { Subscription } from 'rxjs';
import { SessionService } from '../services/session.service';
//import { Subscription } from 'rxjs/Subscription';



@Component({
    selector: 'app-startperfios',
    templateUrl: './startperfios.component.html',
    styleUrls: ['./startperfios.component.css']
})
export class StartPerfiosComponent implements OnInit, OnDestroy {
   payload: string;
    signature: string;
   constructor(private sessionService: SessionService) {
            this.payload = "";
            this.signature = "";
   }

    ngOnInit() {
       this.payload = this.sessionService.getPerfiosPayload();
       this.signature = this.sessionService.getPerfiosSignature();
    }

    ngOnDestroy() {

    }


}
