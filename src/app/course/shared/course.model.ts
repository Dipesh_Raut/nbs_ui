import { Document } from '../../model/document.model';

export class Course {
	course_level: string;
	course_stream : string;
	admission_status:string;
	university: string;
	college : string;
	course : string ;
	i20 : boolean;
	cas:boolean;
	timeOfStudy:any;
	work_experience : boolean;
	monthly_income : string;
	i20_document:Document;
	cas_document:Document;
}