import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Course } from './course.model';

@Injectable()
export class ContactService {

	constructor(private http: Http) { }

	getList(): Observable<Course[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Course[]));
	}
	postOrder(cart): Observable<Course[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Course[]));
	}
}