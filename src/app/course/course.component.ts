import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../header.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Location} from '@angular/common';
import { Course } from './shared/course.model';
import { SaveCaseService } from '../services/saveCase.service';
import { BasicDetails } from '../model/basicDetails.model';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LookupService } from '../services/lookup.service';
import { Document } from '../model/document.model';


@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  /* featured1 = '';
  featured2 = '';
  featured3 = '';
  featured4 = '';
  featured5 = '';
  college = '';
  i20 = '';
  school = '';
  high_school = '';
  graduation = '';
  score = '';
  work_experience = '';
  monthly_income = '';
  courses = '' ;
  entrance_exam ='';
  courseStreams:any;
  disabled :any;
  admissionStatus:any;
  closeResult='';*/
  universities = ["FTMS GLOBAL ACADEMY","FULL SAIL UNIVERSITY","FAIRLEIGH DICKINSON UNIVERSITY - CanadaADA"]; 
  course: Course;
  closeResult='';
  courseStreams:any;
  admissionStatus:any;
  disabled :any;

  basicDetails: BasicDetails;
  courseDetailSubscripton: Subscription;
  basicDetialsSubscription: Subscription;
  saveCaseSubscription: Subscription;
  course_levels:string[];
  admit:boolean;
  i20:boolean;
  cas:boolean;

constructor(
  private router: Router,
  private headerService: HeaderService,
  private modalService: NgbModal,
  private _location: Location,
  private saveCaseService: SaveCaseService,
  private lookupService: LookupService,
  private toastr: ToastrService) {
    this.admit = false;
    this.i20 = false;
    this.cas = false;
    this.course = new Course();
  this.courseStreams = [
    'Medicine',
    'Engineering',
    'Certificate',
    'Management',
    'Agriculture',
 ]; 
 this.admissionStatus = [
  'Confirmed',
  'Awaiting',
  'Not Applied',
    ];
 }

 educationInfoHide(value)
 {
 console.log(value);
 }
public courcePreocess()
{
  console.log(this.course);
  this.saveCaseService.setCourseDetails(this.course);
  
}

handleFileSelect(event,type){
  if(type === "admit"){
    this.admit = true;
    this.course.timeOfStudy =  "admit";
  }
  if(type === "i20"){
    this.i20 = true;
    this.course.i20_document =new Document();
    this.course.i20_document.fileName = "i20";
  }
  if(type === "cas"){
    this.cas = true;
    this.course.cas_document =new Document();
    this.course.cas_document.fileName = "cas";
  }

}

ngOnInit() {
  this.headerService.setTitle('Course Details');
  this.subscribeToBasicDetails();
  this.subscribeToCourseDetails();
  this.subscribeToSaveCase();
  this.saveCaseService.getBasicDetails();
  this.saveCaseService.getCourseDetails();
  this.lookupService.getLookupDetails('courseLevel').subscribe(
    response => {
       this.course_levels = response;
    },
    error => {
       console.log(error);
    }
 );
  
}

ngOnDestroy() {
  if (this.courseDetailSubscripton && !this.courseDetailSubscripton.closed) {
    this.courseDetailSubscripton.unsubscribe();
  }

  if (this.basicDetialsSubscription && !this.basicDetialsSubscription.closed) {
    this.basicDetialsSubscription.unsubscribe();
  }
  if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
    this.saveCaseSubscription.unsubscribe();
  }
}

subscribeToCourseDetails(){
  this.courseDetailSubscripton = this.saveCaseService.subjectCourseDetails.subscribe(
    next => {
      this.course = next;
      if(this.course.timeOfStudy === "admit"){
        this.admit = true;
      }
      if(this.course.i20_document){
        if(this.course.i20_document.fileName === "i20"){
          this.i20 = true;
        }
      }
      if(this.course.cas_document){
        if( this.course.cas_document.fileName === "cas"){
          this.cas = true;
        }
      }
    },
    error => {
      console.error('Error while fetching student details', error);
    }
  );  
}

subscribeToBasicDetails() {
  this.basicDetialsSubscription = this.saveCaseService.subjectBasicDetails.subscribe(
    next => {
      this.basicDetails = next;
      //this.panNumber = this.basicDetails.pan;
    },
    error => {
      console.error('Error while fetching basic details', error);
    }
  );
}
subscribeToSaveCase() {
  this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
    next => {
      this.router.navigateByUrl('/co-applicant');
    },
    error => {

    }
  );
}
streamClick(val){
  this.course.course_stream = val;
 // this.disabled = val;
}

admissionStatusClick(val){
  this.course.admission_status = val;
 // this.disabled = val;
}
  isActive(item){
    //return false;
    return this.course.course_stream === item;
  }

  isStatusActive(item){
    return this.course.admission_status === item;
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  backClicked() {
    this._location.back();
  }

}
