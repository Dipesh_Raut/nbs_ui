import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeaderService } from '../header.service';
import { Subscription } from 'rxjs';
import { Case } from '../model/case.model';
import { SaveCaseService } from '../services/saveCase.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit, OnDestroy {

  constructor(
    private headerService: HeaderService,
    private saveCaseService: SaveCaseService
  ) { }

  case: Case = new Case();

  caseSubscription: Subscription;

  ngOnInit() {
    this.headerService.setTitle('Summary');

    this.subscribeToCase();
    this.saveCaseService.getCase();
  }

  ngOnDestroy() {
    if (this.caseSubscription && !this.caseSubscription.closed) {
      this.caseSubscription.unsubscribe();
    }
  }

  subscribeToCase() {
    this.caseSubscription = this.saveCaseService.subjectCase.subscribe(
      next => {
        this.case = next;
      },
      error => {

      }
    );
  }

}
