import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeaderService } from '../header.service';
import { Location } from '@angular/common';
import { SaveCaseService } from '../services/saveCase.service';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs';
import { BasicDetails } from '../model/basicDetails.model';


@Component({
  selector: 'app-newloan',
  templateUrl: './newloan.component.html',
  styleUrls: ['./newloan.component.css']
})
export class NewloanComponent implements OnInit, OnDestroy {

  serviceLocation = ['Mumbai','Chennai','Bengaluru','Pune','Ahmedabad','Delhi','Hyderabad'];
  educationCountry ='';
  disableTextbox =  false;
  disabledButton:any;
  disabledFlag:any;
  hiddenDiv = true;
  hiddenDropDiv = true;
  otherCountries = ['LA','New Zeland','New York'];
  educationType = ['Higher Education','Executive Education'];
  hiddenCounrtyDiv = true;
  hiddenServiceDiv = true;
  CountryFlag =[
    { source: "assets/images/flags/australia.png", name: 'Australia' },
    { source: "assets/images/flags/US.png", name: 'US' },
    { source: "assets/images/flags/canada.png", name: 'Canada' },
    { source: "assets/images/flags/india.png", name: 'India' },
    { source: "assets/images/flags/UK.png", name: 'UK' },
    { source: "assets/images/flags/others.png", name: 'Others' }
  ]


  educCountry: string = '';
  serLocation: any = ['Higher Education','Executive Education'];

  basicDetails: BasicDetails = new BasicDetails();
  basicDetailsSubscription: Subscription;
  saveCaseSubscription: Subscription;

  constructor(
    private headerService: HeaderService,
    private _location: Location,
    private saveCaseService: SaveCaseService,
    private router: Router) { }

  ngOnInit() {
    this.headerService.setTitle('New Loan Requirement');
    this.subscribeToBasicDetails();
    this.subscribeToSaveCase();
    this.saveCaseService.getBasicDetails();
  }

  ngOnDestroy() {
    if (this.basicDetailsSubscription && !this.basicDetailsSubscription.closed) {
      this.basicDetailsSubscription.unsubscribe();
    }
    if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
      this.saveCaseSubscription.unsubscribe();
    }
  }

  backClicked() {
    this._location.back();
  }

  subscribeToBasicDetails() {
    this.basicDetailsSubscription = this.saveCaseService.subjectBasicDetails.subscribe(
      next => {
        this.basicDetails = next;

        // this.educCountry = this.basicDetails.educationCountry;
        // this.serLocation = this.basicDetails.serviceLocation;
      },
      error => {
        console.error('Error while fetching basic details', error);
      }
    );
  }

  subscribeToSaveCase() {
    this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
      next => {
        this.router.navigateByUrl("/one-step-away");
      },
      error => {
        
      }
    );
  }

  onNewLoanProceed() {
    // this.basicDetails = this.saveCaseService.getBasicDetails();
    // this.basicDetails.educationCountry = this.educCountry;
    // this.basicDetails.serviceLocation = this.serLocation;

    this.saveCaseService.setBasicDetails(this.basicDetails);
  }

  setCountryName(value)
  {
    
    if(value!='Others')
    {
      this.disabledFlag = value;
      this.hiddenDiv =false;
      this.basicDetails.educationCountry = value;
      this.disableTextbox = true;
      this.hiddenDropDiv = true;
      this.hiddenServiceDiv =false;
    }
    else{
      this.disabledFlag = value;
      this.hiddenDiv = true;
      this.hiddenDropDiv =false;
      this.hiddenServiceDiv =false;
    }
    
  }
  isActive(item){
    //return false;
    return this.disabledButton === item;
  }
  isImgActive(item){
    return this.disabledFlag === item;
  }
  change_button_class(val)
  {
    this.hiddenCounrtyDiv=false;
    this.disabledButton = val;
  }

}
