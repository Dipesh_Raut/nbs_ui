import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Newloan } from './newloan.model';

@Injectable()
export class NewloanService {

	constructor(private http: Http) { }

	getList(): Observable<Newloan[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Newloan[]));
	}
	postOrder(cart): Observable<Newloan[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Newloan[]));
	}
}