import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { OneStepAway } from './one-step-away.model';
import { environment } from 'src/environments/environment';
import { Document } from '../../model/document.model';

@Injectable()
export class OneStepAwayService {

	constructor(private httpClient: HttpClient) { }
	private httpHeaders = new HttpHeaders({ 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1YjY5MzYzYTc4NGI3ZjRmZjNiNTI2OGQiLCJzdWIiOiJBdXhpbG8iLCJpc3MiOiJrY1FtUnkzMUNOMGhBaXU5bHFYQmRqdHNaaHlxN0ZPVCJ9.QbZmNqIMRDdWyW32Bi7y1t6P1r9KGx95kPhGWzQUSoo',
	 'Content-Type' : 'application/json' });
	 

	getDataFromGoogleVision(base64?: string,type?: string ): Observable<any> {
        const body =  {
			'file': base64,
			'functionInstanceName': 'googleVision',
			'type': type
		}

        return this.httpClient.post(environment.baseUrl + environment.googlevision,
                body,
                {   headers: this.httpHeaders,
                    observe: 'body'})
	}
	
	uploadDocument(document?: Document) {
		const httpHeaders = new HttpHeaders({ 'Authorization': localStorage.getItem('Authorization'),'Content-Type' : 'application/json' });
		return this.httpClient.post<any>(
			environment.uploaddocument,
			document,
			{
				observe: 'body',
				headers: httpHeaders,
				reportProgress: true,
				//withCredentials: true
			}
			)
	}
}
