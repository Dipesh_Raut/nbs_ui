import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HeaderService } from '../header.service';
import { OneStepAwayService } from './shared/one-step-away.service';
import { StudentDetails } from '../student-details/shared/student-details.model';
import { BasicDetails } from '../model/basicDetails.model';
import { Parent } from '../parent/shared/parent.model';
import { SaveCaseService } from '../services/saveCase.service';
import { PancardService} from '../pancard/shared/pancard.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Document } from '../model/document.model';


@Component({
  selector: 'app-one-step-away',
  templateUrl: './one-step-away.component.html',
  styleUrls: ['./one-step-away.component.css']
})
export class OneStepAwayComponent implements OnInit, OnDestroy {
  base64textString: string;
  fileType: string;
  fileName: string;
  aadharCardMessage: string;
  panCardMessage: string;
  passportFirstPageMessage: string;
  passportLastPageMessage: string;
  studentDetails: StudentDetails;
  basicDetails: BasicDetails;
  parentDetails: Parent;
  proceedFlag:boolean;

  studentDetailsSubscription: Subscription;
  parentDetailsSubscription: Subscription;
  saveCaseSubscription: Subscription;

  //parentDetails:
  constructor(
    private modalService: NgbModal,
    private headerService: HeaderService,
    private oneStepService: OneStepAwayService,
    private saveCaseService: SaveCaseService,
    private panCardService: PancardService,
    private router: Router
  ) {
    this.base64textString = '';
    this.fileType = '';
    this.fileName = '';
    this.proceedFlag = false;
  }

  ngOnInit() {

    this.headerService.setTitle('One Step Away');

    this.subscribeToStudentDetails();
    this.subscribeToParentDetails();
    this.subscribeToSaveCase();
    this.saveCaseService.getStudentDetails();
    this.saveCaseService.getParentDetails();
    this.aadharCardMessage = "Please upload Aadhar Card";
    this.panCardMessage = 'Please Upload Pan Card';
    this.passportFirstPageMessage = 'Please Upload Passport First Page';
    this.passportLastPageMessage = 'Please Upload Passport Last Page';
    

  }

  ngOnDestroy() {
    if (this.studentDetailsSubscription && !this.studentDetailsSubscription.closed) {
      this.studentDetailsSubscription.unsubscribe();
    }
    if (this.parentDetailsSubscription && !this.parentDetailsSubscription.closed) {
      this.parentDetailsSubscription.unsubscribe();
    }
    if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
      this.saveCaseSubscription.unsubscribe();
    }
  }

  subscribeToStudentDetails() {
    this.studentDetailsSubscription = this.saveCaseService.subjectStudentDetails.subscribe(
      next => {
        this.studentDetails = next;

      },
      error => {
        console.error('Error while fetching student details', error);
      }
    );
  }

  subscribeToParentDetails() {
    this.parentDetailsSubscription = this.saveCaseService.subjectParentDetails.subscribe(
      next => {
        this.parentDetails = next;
      },
      error => {
        console.error('Error while fetching parent details', error);
      }
    );
  }

  closeResult = '';
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  handleFileSelect(evt, type) {
    let files = evt.target.files;
    const file = files[0];
    this.fileType = type
    this.fileName = file.name;
    console.log(file);
    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.oneStepService.getDataFromGoogleVision(this.base64textString, this.fileType).subscribe(response => {
      if (response) {
        console.log(response);
        if (this.fileType === 'identity document') {
          if (this.studentDetails.first_name == null || this.studentDetails.first_name.trim().length === 0) {
            this.studentDetails.first_name = response.name;
          }
          this.studentDetails.gender = response.gender;
          this.studentDetails.currentAddress = response.address;
          if(response.dateOfBirth && response.dateOfBirth.trim().length > 0) {
            const arr = response.dateOfBirth.split("/");
            if (arr.length === 3){
              this.studentDetails.dateOfBirth = arr[2] + '-' + arr[1] + '-' + arr[0];
            }
          }
          const document = new Document();
          document.content = this.base64textString;
          document.belongsTo = 'applicant';
          document.type = 'Aadhar';
          document.fileName = this.fileName;
          this.oneStepService.uploadDocument(document).subscribe(docRes => {
            if(docRes.success){
              this.studentDetails.aadharReceived = true;
              this.saveCaseService.setStudentDetails(this.studentDetails);
            }
          });
        }
        if (this.fileType === 'pan') {
          const document = new Document();
          document.content = this.base64textString;
          document.belongsTo = 'applicant';
          document.type = 'Pan';
          document.fileName = this.fileName;
          if (response.panNumber && response.panNumber.trim().length > 0) {
            this.panCardService.panVlidation(response.panNumber).subscribe( data => {
              if(data){
                if (data.panValidity === 'E') {
                  this.studentDetails.first_name = data.fName;
                  this.studentDetails.middle_name = data.middleName;
                  this.studentDetails.last_name = data.lName;
                  this.studentDetails.panNumber = data.panNo;
                  this.studentDetails.panVerified = true;
                  this.oneStepService.uploadDocument(document).subscribe(docRes => {
                    if(docRes.success){
                      this.studentDetails.panReceived = true;
                      this.saveCaseService.setStudentDetails(this.studentDetails);
                    }
                  });
                } else {
                  this.studentDetails.panVerified = false;
                  this.studentDetails.panReceived = false;
                  this.saveCaseService.setStudentDetails(this.studentDetails);
                }
              }
            });
          }
        }
        if (this.fileType === 'passportFirstPage') {
          this.studentDetails.passportNumber = response.passportNum;
          if (this.studentDetails.last_name == null || this.studentDetails.last_name.trim().length === 0) {
            this.studentDetails.last_name = response.surname;
          }
          if (this.studentDetails.first_name == null || this.studentDetails.first_name.trim().length === 0) {
            if(response.fname != null && response.fname.trim().length > 0){
              this.studentDetails.first_name = response.fname;
            }
            this.studentDetails.last_name = response.surname;
          }
          const document = new Document();
          document.content = this.base64textString;
          document.belongsTo = 'applicant';
          document.type = 'passportFirstPage';
          document.fileName = this.fileName;
          this.oneStepService.uploadDocument(document).subscribe(docRes => {
            if (docRes.success) {
              this.studentDetails.passportFirstPageReceived = true;
              this.saveCaseService.setStudentDetails(this.studentDetails);
            }
          });
        }
        if (this.fileType === 'passportLastPage') {
          if (response.fatherName) {
            this.parentDetails.father_first_name = response.fatherName.split(' ')[0];
            this.parentDetails.father_last_name = response.fatherName.split(' ')[1];
          }
          if (response.motherName) {
            this.parentDetails.mother_first_name = response.motherName.split(' ')[0];
            this.parentDetails.mother_last_name = response.motherName.split(' ')[1];
          }
          const document = new Document();
          document.content = this.base64textString;
          document.belongsTo = 'applicant';
          document.type = 'passportFirstPage';
          document.fileName = this.fileName;
          this.oneStepService.uploadDocument(document).subscribe(docRes => {
            if (docRes.success){
              this.studentDetails.passportLastPageReceived = true;
              this.saveCaseService.setStudentDetails(this.studentDetails);
            }
          });
        }
      }
    });
  }

  proceed() {
    this.proceedFlag = true;
    this.saveCaseService.setStudentDetails(this.studentDetails, true);
    this.saveCaseService.setParentDetails(this.parentDetails);
  }

  subscribeToSaveCase() {
    this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
      next => {
        if(this.proceedFlag){
          this.router.navigateByUrl('/student-details');
        }
      },
      error =>{

      }
    );
  }
}
