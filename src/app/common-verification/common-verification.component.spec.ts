import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonVerificationComponent } from './common-verification.component';

describe('CommonVerificationComponent', () => {
  let component: CommonVerificationComponent;
  let fixture: ComponentFixture<CommonVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
