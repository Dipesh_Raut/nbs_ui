import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-verify-aadhaarcard',
  templateUrl: './verify-aadhaarcard.component.html',
  styleUrls: ['./verify-aadhaarcard.component.css']
})
export class VerifyAadhaarcardComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Verify Aadhaarcard');
  }

}
