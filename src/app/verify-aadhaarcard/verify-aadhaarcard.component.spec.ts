import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyAadhaarcardComponent } from './verify-aadhaarcard.component';

describe('VerifyAadhaarcardComponent', () => {
  let component: VerifyAadhaarcardComponent;
  let fixture: ComponentFixture<VerifyAadhaarcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyAadhaarcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyAadhaarcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
