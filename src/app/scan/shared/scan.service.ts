import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Scan } from './scan.model';

@Injectable()
export class ScanService {

	constructor(private http: Http) { }

	getList(): Observable<Scan[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Scan[]));
	}
	postOrder(cart): Observable<Scan[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Scan[]));
	}
}