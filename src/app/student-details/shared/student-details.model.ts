import { Document } from '../../model/document.model';

export class StudentDetails {
	first_name : string;
	middle_name : string;
	last_name : string;
	mobile : string;
	email : string;
	panNumber : string;
	school : string;
	high_school : string;
	graduation : string;
	score : string;
	gender : string = "Male";
	aadhar_card : Document;
	aadharReceived: boolean;
	pan_card : Document;
	panReceived:boolean;
	voter_id : Document;
	voterIdReceived:boolean;
	income_proof : Document;
	examName : any;
	bank_list:any;
	entranceExamAndScore = [];
	loan_amount : string = '100000';
	kyc_documents: Document[];
	panVerified:boolean;
	bankName:string;
	bankStatements:Document[];
	currentAddress: string;
	permenantAddress: string;
	dateOfBirth: string;
	passportNumber: string;
	itrReceived:boolean;
	bankStatementReceived:boolean;
	passportFirstPageReceived:boolean;
	passportLastPageReceived:boolean;
}