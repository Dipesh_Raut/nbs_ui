import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Options } from 'ng5-slider';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HeaderService } from '../header.service';
import { Location,CurrencyPipe } from '@angular/common';
import { StudentDetails } from './shared/student-details.model';
import { SaveCaseService } from '../services/saveCase.service';
import {  OneStepAwayService } from '../one-step-away/shared/one-step-away.service';
import { Router } from '@angular/router';
import { StudentDetailsService } from './shared/student-details.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { BasicDetails } from '../model/basicDetails.model';
import { Document } from '../model/document.model';
import { SessionService } from '../services/session.service';

class ExamScore {
  exam: string = '';
  score: string = '';

}

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css'],
  providers: [CurrencyPipe]
})
export class StudentDetailsComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onToggle = new EventEmitter();
  @Output() payload = new EventEmitter();
  @Output() signature = new EventEmitter();


  studentDetails: StudentDetails;
  basicDetails: BasicDetails;
  entranceExmScores: ExamScore[] = [new ExamScore()];
  perfiosUrl: string;
  perfiosTransactionId: string;
  perfiosTransactionCompleted: string;
  perfiosTransactionFailure: string;
  perfiosTransactionTimeout: string;
  perfiosTransactionCancelled: string;
  perfiosTransactionError: string;
  proceedFlag:boolean;

  examsName = ['GRE', 'GMAT', 'IELTS', 'TOFEL', 'SAT', 'ACT', 'PTE', 'CAT', 'XAT', 'CMAT', 'MAT', 'ATMA', 'NMAT', 'SNAP', 'CET'];
  entranceExam = '';
  bank_list = [];
  closeResult: string;

 
  studentDetailsSubscription: Subscription;
  basicDetailsSubscription: Subscription;
  saveCaseSubscription: Subscription;

  fileType: string;
  fileName: string;
  base64textString: string;

  aadharCardMessage: string;
  panCardMessage: string;
  voterIdMessage: string;
  disableAadhar: boolean;
  disablePan: boolean;
  disableVoter: boolean;
  loan_amount : string = '100000';

 

  // value: number = 500000;
  options: Options = {
    floor: 500000,
    ceil: 5000000,
    step: 500000
  };
  constructor(
    private modalService: NgbModal,
    private headerService: HeaderService,
    private _location: Location,
    private saveCaseService: SaveCaseService,
    private studentDetailsService: StudentDetailsService,
    private oneStepService: OneStepAwayService,
    private sanitizer: DomSanitizer,
    private sessionService: SessionService,
    private router: Router,
    private cp: CurrencyPipe) {
    this.studentDetails = new StudentDetails();
    this.perfiosTransactionId = '';
    this.perfiosUrl = '';
    this.perfiosTransactionCompleted = 'COMPLETED';
    this.perfiosTransactionCancelled = 'CANCELLED';
    this.perfiosTransactionError = 'ERROR';
    this.perfiosTransactionFailure = 'REPORT_DELIVERY_FAILED';
    this.perfiosTransactionTimeout = 'TIMEOUT';
    this.proceedFlag = false;

  }

  // get entranceExams() {
  // return this.myForm.get('entranceExams') as FormArray;
  // } 
  // onRupee(event:any){
  //     if(event.which >= 37 && event.which <= 40) return;

  //     // format number
  //     $(this).val(function(index, value) {
  //       return value
  //       .replace(/\D/g, "")
  //       .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  //       ;
  //     });
  // }
  addExamFileds() {
    this.studentDetails.entranceExamAndScore.push(new ExamScore());
  }

  removeExamFileds(i: number) {
    this.studentDetails.entranceExamAndScore.splice(i, 1);
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  openItr(subcontentitr) {
    let panNumber;
    if (this.basicDetails.pan) {
      panNumber = this.basicDetails.pan;
    } else {
      panNumber = this.studentDetails.panNumber;
    }
    if (panNumber && this.studentDetails.dateOfBirth) {
      this.studentDetailsService.perfiosFetchStart(panNumber, this.studentDetails.dateOfBirth).subscribe(
        (response: any) => {
          console.log(response);
          if (response) {
            this.perfiosUrl = response.url;
            this.perfiosTransactionId = response.transactionId;

            this.modalService.open(subcontentitr, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
              this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
          }
        }
      );
    }
  }
  openBank(subcontentbank) {
    this.modalService.open(subcontentbank, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openIframeITR(contentIframeITR) {
    this.sessionService.setApplicantType("student");
    window.open(this.perfiosUrl);

  }

  openIframeBank(contentIframeBank, type) {
    if (this.basicDetails.email) {
      console.log(type);
      console.log(this.basicDetails.email);
      this.studentDetailsService.bankFetch(type, "manish@automaatapi.com").subscribe(
        (response: any) => {
          if (response) {
            this.sessionService.setPerfiosPayload(response.paylaod);
            this.sessionService.setPerfiosSignature(response.signature);
            this.sessionService.setApplicantType("student");
            console.log(response);
            window.open("https://auxiloportal.automatapi.xyz/#/startperfios");
          }
        });
      // this.modalService.open(contentIframeBank, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      //   this.closeResult = `Closed with: ${result}`;
      // }, (reason) => {
      //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      // });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  studentProcess() {
    this.proceedFlag = true;
    this.saveCaseService.setBasicDetails(this.basicDetails, true);
    this.saveCaseService.setStudentDetails(this.studentDetails);
    
  }

  ngOnInit() {
    this.headerService.setTitle('Student Details');
    this.perfiosUrl = '';
    this.subscribeToBasicDetails();
    this.subscribeToStudentDetails();
    this.subscribeToSaveCase();
    this.saveCaseService.getBasicDetails();
    this.saveCaseService.getStudentDetails();
  }

  ngOnDestroy() {
    if (this.basicDetailsSubscription && !this.basicDetailsSubscription.closed) {
      this.basicDetailsSubscription.unsubscribe();
    }
    if (this.studentDetailsSubscription && !this.studentDetailsSubscription.closed) {
      this.studentDetailsSubscription.unsubscribe();
    }
    if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
      this.saveCaseSubscription.unsubscribe();
    }
  }

  subscribeToBasicDetails() {
    this.basicDetailsSubscription = this.saveCaseService.subjectBasicDetails.subscribe(
      next => {
        this.basicDetails = next;
      },
      error => {
        console.error('Error while fetching basic details', error);
      }
    );
  }

  subscribeToStudentDetails() {
    this.studentDetailsSubscription = this.saveCaseService.subjectStudentDetails.subscribe(
      next => {
        this.studentDetails = next;
      },
      error => {
        console.error('Error while fetching student details', error);
      }
    );
  }

  subscribeToSaveCase() {
    this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
      next => {
        if(this.proceedFlag){
          this.router.navigateByUrl('/course');
        }
      },
      error => {

      }
    );
  }

  backClicked() {
    this._location.back();
  }

  handleFileSelect(evt, type) {
    let files = evt.target.files;
    const file = files[0];
    this.fileType = type
    this.fileName = file.name;
    console.log(file);
    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);

    if (this.fileType === 'aadhar') {
      const document = new Document();
      document.content = this.base64textString;
      document.belongsTo = 'applicant';
      document.type = 'Aadhar';
      document.fileName = this.fileName;
      this.oneStepService.uploadDocument(document).subscribe(docRes => {
        if(docRes.success){
          this.studentDetails.aadharReceived = true;
          this.saveCaseService.setStudentDetails(this.studentDetails);
        }
      });
    }
    if (this.fileType === 'pan') {
      const document = new Document();
      document.content = this.base64textString;
      document.belongsTo = 'applicant';
      document.type = 'pan';
      document.fileName = this.fileName;
      this.oneStepService.uploadDocument(document).subscribe(docRes => {
        if(docRes.success){
          this.studentDetails.panReceived = true;
          this.saveCaseService.setStudentDetails(this.studentDetails);
        }
      });
    }
    if (this.fileType === 'voter') {
      const document = new Document();
      document.content = this.base64textString;
      document.belongsTo = 'applicant';
      document.type = 'voterId';
      document.fileName = this.fileName;
      this.oneStepService.uploadDocument(document).subscribe(docRes => {
        if(docRes.success){
          this.studentDetails.voterIdReceived = true;
          this.saveCaseService.setStudentDetails(this.studentDetails);
        }
      });
    }
  }
}
