"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var operators_1 = require("rxjs/operators");
var config_1 = require("../../shared/config");
var PancardService = /** @class */ (function () {
    function PancardService(http) {
        this.http = http;
    }
    PancardService.prototype.getList = function () {
        return this.http.get('/api/list').pipe(operators_1.map(function (res) { return res.json(); }));
    };
    PancardService.prototype.postOrder = function (cart) {
        return this.http.post(config_1.Config.apiURL + '/cart', cart).pipe(operators_1.map(function (res) { return res.json(); }));
    };
    PancardService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], PancardService);
    return PancardService;
}());
exports.PancardService = PancardService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFjdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNDQUFxQztBQUVyQyw0Q0FBcUM7QUFDckMsOENBQTZDO0FBSTdDO0lBRUMsd0JBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO0lBQUksQ0FBQztJQUVuQyxnQ0FBTyxHQUFQO1FBQ0MsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsSUFBSSxFQUFlLEVBQXZCLENBQXVCLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFDRCxrQ0FBUyxHQUFULFVBQVUsSUFBSTtRQUNiLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBRSxlQUFNLENBQUMsTUFBTSxHQUFHLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBRyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLElBQUksRUFBZSxFQUF2QixDQUF1QixDQUFDLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBVFcsY0FBYztRQUQxQixpQkFBVSxFQUFFO3lDQUdjLFdBQUk7T0FGbEIsY0FBYyxDQVUxQjtJQUFELHFCQUFDO0NBQUEsQUFWRCxJQVVDO0FBVlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uZmlnJztcbmltcG9ydCB7IENvbnRhY3QgfSBmcm9tICcuL2NvbnRhY3QubW9kZWwnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ29udGFjdFNlcnZpY2Uge1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cCkgeyB9XG5cblx0Z2V0TGlzdCgpOiBPYnNlcnZhYmxlPENvbnRhY3RbXT4ge1xuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0KCcvYXBpL2xpc3QnKS5waXBlKG1hcChyZXMgPT4gcmVzLmpzb24oKSBhcyBDb250YWN0W10pKTtcblx0fVxuXHRwb3N0T3JkZXIoY2FydCk6IE9ic2VydmFibGU8Q29udGFjdFtdPiB7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0KCBDb25maWcuYXBpVVJMICsgJy9jYXJ0JywgY2FydCkucGlwZShtYXAocmVzID0+IHJlcy5qc29uKCkgYXMgQ29udGFjdFtdKSk7XG5cdH1cbn0iXX0=