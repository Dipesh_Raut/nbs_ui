import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { environment } from 'src/environments/environment';



@Injectable({providedIn : "root"})
export class PancardService {

	constructor(private httpClient: HttpClient) {
      
    }

	panVlidation(pan?: string) {
		const httpHeaders = new HttpHeaders({ 'Authorization': localStorage.getItem('Authorization'),'Content-Type' : 'application/json' });
	  const body = {
		  'functionInstanceName':"nsdlPan",
		  'pan':pan
	  }
	  return this.httpClient.post<any>(
		 environment.validatepan,
		  body,
		  {
			observe: 'body',
			headers: httpHeaders,
			reportProgress: true,
			//withCredentials: true
		  }
		)
	}
}
