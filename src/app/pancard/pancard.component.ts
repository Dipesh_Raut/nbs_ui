import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeaderService } from '../header.service';
import { SaveCaseService } from '../services/saveCase.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BasicDetails } from '../model/basicDetails.model';
import { StudentDetails } from '../student-details/shared/student-details.model';
import { Subscription } from 'rxjs';
import { PancardService} from './shared/pancard.service';
//import { SweetAlertService } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-pancard',
  templateUrl: './pancard.component.html',
  styleUrls: ['./pancard.component.css']
})
export class PancardComponent implements OnInit, OnDestroy {

  panNumber = '';
  //showRegSuccess: boolean=false;

  basicDetails: BasicDetails = new BasicDetails();
  studentDetails: StudentDetails = new StudentDetails();

  basicDetialsSubscription: Subscription;
  studentDetailsSubscription: Subscription;
  saveCaseSubscription: Subscription;

  constructor(
    private headerService: HeaderService,
    private saveCaseService: SaveCaseService,
    private pancardService: PancardService,
    private router: Router,
    private toastr: ToastrService ) { }

  ngOnInit() {
    this.headerService.setTitle('');
    this.subscribeToBasicDetails();
    this.subscribeToStudentDetails();
    this.subscribeToSaveCase();
    this.saveCaseService.getBasicDetails();
    this.saveCaseService.getStudentDetails();
  }

  ngOnDestroy() {
    if (this.basicDetialsSubscription && !this.basicDetialsSubscription.closed) {
      this.basicDetialsSubscription.unsubscribe();
    }
    if (this.studentDetailsSubscription && !this.studentDetailsSubscription.closed) {
      this.studentDetailsSubscription.unsubscribe();
    }
    if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
      this.saveCaseSubscription.unsubscribe();
    }
  }

  subscribeToBasicDetails() {
    this.basicDetialsSubscription = this.saveCaseService.subjectBasicDetails.subscribe(
      next => {
        this.basicDetails = next;
        this.panNumber = this.basicDetails.pan;
      },
      error => {
        console.error('Error while fetching basic details', error);
      }
    );
  }

  subscribeToStudentDetails() {
    this.studentDetailsSubscription = this.saveCaseService.subjectStudentDetails.subscribe(
      next => {
        this.studentDetails = next;
        console.log(this.studentDetails);
      },
      error => {
        console.error('Error while fetching student details', error);
      }
    );
  }

  subscribeToSaveCase() {
    this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
      next => {
        this.toastr.success('Authentication Successfull', 'Congratulation!');
        this.router.navigateByUrl('/loan-purpose');
      },
      error => {

      }
    );
  }

  onPanClick() {
    this.basicDetails.pan = this.panNumber;
    this.saveCaseService.setBasicDetails(this.basicDetails, true);
    if (this.panNumber.trim().length !== 0) {
      this.pancardService.panVlidation(this.panNumber).subscribe(response => {
        console.log(response);
        if( response.panValidity === 'E'){
          this.studentDetails.first_name = response.fName;
          this.studentDetails.middle_name = response.middleName;
          this.studentDetails.last_name = response.lName;
          this.studentDetails.panNumber = this.panNumber;
          this.studentDetails.panVerified = true;
          this.saveCaseService.setStudentDetails(this.studentDetails);
        }
      });
    }
  }
  skipProcess(){
    this.toastr.success('Authentication Successfull', 'Congratulation!');
    this.router.navigateByUrl('/loan-purpose');
  }

}
