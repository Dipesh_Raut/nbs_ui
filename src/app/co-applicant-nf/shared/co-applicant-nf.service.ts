import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { CoApplicantNf } from './co-applicant-nf.model';

@Injectable()
export class CoApplicantNfService {

	constructor(private http: Http) { }

	getList(): Observable<CoApplicantNf[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as CoApplicantNf[]));
	}
	postOrder(cart): Observable<CoApplicantNf[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as CoApplicantNf[]));
	}
}