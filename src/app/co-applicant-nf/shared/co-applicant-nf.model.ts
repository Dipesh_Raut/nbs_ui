export class CoApplicantNf {
	first_name : string;
	middle_name : string;
	last_name : string;
	relationship : string;
	mobile : string;
	email : string;
	net_monthly_income : number;
	occupation : string;
	gender : string;
}