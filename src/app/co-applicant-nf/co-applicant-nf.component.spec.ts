import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoApplicantNfComponent } from './co-applicant-nf.component';

describe('CoApplicantNfComponent', () => {
  let component: CoApplicantNfComponent;
  let fixture: ComponentFixture<CoApplicantNfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoApplicantNfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoApplicantNfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
