import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';
import {Location} from '@angular/common';


@Component({
  selector: 'app-co-applicant-nf',
  templateUrl: './co-applicant-nf.component.html',
  styleUrls: ['./co-applicant-nf.component.css']
})
export class CoApplicantNfComponent implements OnInit {

   
  first_name = '';
  middle_name = '';
  last_name = '';
  relationship = '';
  mobile = '';
  email = '';
  net_monthly_income = '';
  occupation = '';
  gender = '';

  constructor(private headerService: HeaderService, private _location: Location) { }

  ngOnInit() {
    this.headerService.setTitle('Co-Applicant Non Finacial');
  }

  backClicked() {
    this._location.back();
  }

  coApplicantProcess() {
    
  }

}
