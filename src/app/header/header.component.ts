import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';
import { VerificationService } from '../services/verification.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title='auxilo';
  isLoggedIn:boolean =false;
  onLoginSubscriber: Subscription;

  constructor(
    private headerService: HeaderService,
    private verificationService: VerificationService,
    private router: Router) { }

  ngOnInit() {
    this.isLoggedIn = Boolean(localStorage.getItem('Authorization'));
    this.headerService.title.subscribe(title => {
      this.title = title;
    });

    this.onLoginSubscriber = this.verificationService.onLogin.subscribe(()=>{
      this.isLoggedIn =true;
    })
  }

  logout(){
    this.verificationService.logout();
    this.isLoggedIn = false;
    this.router.navigateByUrl('');
    }

  ngOnDestroy() {
    if (this.onLoginSubscriber && !this.onLoginSubscriber.closed) {
      this.onLoginSubscriber.unsubscribe();
    }
  }
}
