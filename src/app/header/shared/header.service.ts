import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Header } from './header.model';

@Injectable()
export class HeaderService {

	constructor(private http: Http) { }

	getList(): Observable<Header[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Header[]));
	}
	postOrder(cart): Observable<Header[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Header[]));
	}
}