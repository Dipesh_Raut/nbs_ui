import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';
@Component({
  selector: 'app-loan-purpose',
  templateUrl: './loan-purpose.component.html',
  styleUrls: ['./loan-purpose.component.css']
})
export class LoanPurposeComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('');
  }

}
