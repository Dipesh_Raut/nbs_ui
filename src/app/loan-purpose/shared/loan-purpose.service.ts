import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { LoanPurpose } from './loan-purpose.model';

@Injectable()
export class LoanPurposeService {

	constructor(private http: Http) { }

	getList(): Observable<LoanPurpose[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as LoanPurpose[]));
	}
	postOrder(cart): Observable<LoanPurpose[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as LoanPurpose[]));
	}
}