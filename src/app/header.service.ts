import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor() { }

  public title = new BehaviorSubject('Title');

  setTitle(title) {
    this.title.next(title);
  } 

}
