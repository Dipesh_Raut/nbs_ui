import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { environment } from 'src/environments/environment';


@Injectable()
export class CoApplicantService {

	private case = null;
    constructor(private httpClient: HttpClient) {
      
    }

    bankFetch(type?: string,emailId?: string) {
		const httpHeaders = new HttpHeaders({ 'Authorization': localStorage.getItem('Authorization'),'Content-Type' : 'application/json' });
		const body = {
			'functionInstanceName':"auxilo_perfios_startStatementAnalysis",
			'destination':type,
			'emailId':emailId
		}
		return this.httpClient.post<any>(
			environment.perfiosbank,
			body,
			{
				observe: 'body',
				headers: httpHeaders,
				reportProgress: true,
				//withCredentials: true
			}
			)
	}
	
	perfiosFetchStart(panNumber?: string, dob?: string) {
		const httpHeaders = new HttpHeaders({ 'Authorization': localStorage.getItem('Authorization'),'Content-Type' : 'application/json' });
	  const body = {
		  'functionInstanceName':"auxilo_perfios_startFetchITR",
		  'panNumber':panNumber,
		  'dateOfBirth':dob
	  }
	  return this.httpClient.post<any>(
		 environment.perfiostransaction,
		  body,
		  {
			observe: 'body',
			headers: httpHeaders,
			reportProgress: true,
			//withCredentials: true
		  }
		)
  }
}