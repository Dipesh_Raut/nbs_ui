import { Document } from '../../model/document.model';

export class CoApplicant {
	first_name : string;
	middle_name : string;
	last_name : string;
	relationship : string;
	mobile : string;
	email : string;
	panNumber:string;
	net_monthly_income : number;
	security_collateral : boolean;
	security_type : string;
	occupation : string;
	gender : string;
	bank_list:any;
	aadhar_card : Document;
	aadharReceived: boolean;
	panReceived: boolean;
	voterIdReceived: boolean;
	pan_card : Document;
	voter_id : Document;
	income_proof : Document;
	bankName:string;
	bankStatements:Document[];
	itrReceived:boolean;
	bankStatementReceived:boolean;
	dateOfBirth:string;
	current_address:string;
	parmanent_address:string;
	
}