import { Component, OnInit,OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HeaderService } from '../header.service';
import { SaveCaseService } from '../services/saveCase.service';
import {Location} from '@angular/common';
import { CoApplicant } from './shared/co-applicant.model';
import { CoApplicantService } from './shared/co-applicant.service';
import { Subscription } from 'rxjs';
import { BasicDetails } from '../model/basicDetails.model';
import { StudentDetails } from '../student-details/shared/student-details.model';
import { Document } from '../model/document.model';
import {  OneStepAwayService } from '../one-step-away/shared/one-step-away.service';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-co-applicant',
  templateUrl: './co-applicant.component.html',
  styleUrls: ['./co-applicant.component.css']
})
export class CoApplicantComponent implements OnInit, OnDestroy {
  
  coApplicant : CoApplicant;
 /*  first_name = '';
  middle_name = '';
  last_name = '';
  relationship = '';
  mobile = '';
  email = '';
  net_monthly_income = '';
  security_collateral = false;
  security_type = '';
  occupation = '';
  gender = '';
  bank_list=[]; */

  closeResult ='';
  bank_list=[];
  occupations = ['SALARIED','SELF EMPLOYED NON PROFESSIONAL(SENP)','SELF EMPLOYED PROFESSIONAL(SEP)','HOMEMAKER','RETIRED'];
  perfiosUrl: string;
  perfiosTransactionId: string;
  studentDetails: StudentDetails;
  basicDetails: BasicDetails;
  studentDetailsSubscription: Subscription;
  basicDetailsSubscription: Subscription;
  coApplicantDetailsSubscription: Subscription;
  saveCaseSubscription: Subscription;
  aadharCardMessage: string;
  panCardMessage: string;
  voterIdMessage:string;
  fileName: string;
  fileType: string;
  base64textString: string;


  current_address ='';
  parmanent_address='';
  sameAsCurrentAdd:boolean=false;


  proceedFlag: boolean;
constructor(
  private router: Router,
  private modalService: NgbModal,
  private headerService: HeaderService,
  private _location: Location,
  private saveCaseService: SaveCaseService,
  private coApplicantService: CoApplicantService,
  private oneStepService: OneStepAwayService,
  private sessionService: SessionService
) 
{ 
  this.coApplicant = new CoApplicant();
  this.aadharCardMessage = "Please upload Aadhar Card";
  this.panCardMessage = 'Please Upload Pan Card';
  this.voterIdMessage = 'Please Upload Voter Id';
  this.perfiosTransactionId = '';
  this.perfiosUrl = '';
  this.base64textString = '';
  this.fileName = '';
  this.fileType = '';
  this.proceedFlag = false;
}

public coApplicantProcess()
{
  this.proceedFlag = true;
  this.saveCaseService.setcoApplicantDetails(this.coApplicant);
}
open(content) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

openItr(subcontentitr) {
  let panNumber;
  panNumber = this.coApplicant.panNumber;
  if (panNumber && this.coApplicant.dateOfBirth) {
    this.coApplicantService.perfiosFetchStart(panNumber, this.coApplicant.dateOfBirth).subscribe(
      (response: any) => {
        console.log(response);
        if (response) {
          this.perfiosUrl = response.url;
          this.perfiosTransactionId = response.transactionId;
          this.modalService.open(subcontentitr, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          });
        }
      }
    );
  }
}
openBank(subcontentbank,type) {
  this.modalService.open(subcontentbank, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

openIframeITR(contentIframeITR) {
  this.sessionService.setApplicantType("coapplicant")
  window.open(this.perfiosUrl);
}

openIframeBank(contentIframeBank, type) {
  if (this.basicDetails.email) {
    this.coApplicantService.bankFetch(type,this.basicDetails.email).subscribe(
      (response: any) => {
        if (response) {
          this.sessionService.setPerfiosPayload(response.paylaod);
          this.sessionService.setPerfiosSignature(response.signature);
          this.sessionService.setApplicantType("coapplicant");
          window.open("https://auxiloportal.automatapi.xyz/#/startperfios");
        }
      });
  }
  // this.modalService.open(contentIframeBank, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
  //   this.closeResult = `Closed with: ${result}`;
  // }, (reason) => {
  //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  // });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}


ngOnDestroy() {
  if (this.basicDetailsSubscription && !this.basicDetailsSubscription.closed) {
    this.basicDetailsSubscription.unsubscribe();
  }
  if (this.studentDetailsSubscription && !this.studentDetailsSubscription.closed) {
    this.studentDetailsSubscription.unsubscribe();
  }
  if (this.coApplicantDetailsSubscription && !this.coApplicantDetailsSubscription.closed) {
    this.coApplicantDetailsSubscription.unsubscribe();
  }
  if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
    this.saveCaseSubscription.unsubscribe();
  }
}

subscribeToSaveCase() {
  this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
    next => {
      if(this.proceedFlag){
        this.router.navigateByUrl('/parent');
      }
    },
    error => {

    }
  );
}

subscribeToBasicDetails() {
  this.basicDetailsSubscription = this.saveCaseService.subjectBasicDetails.subscribe(
    next => {
      this.basicDetails = next;
    },
    error => {
      console.error('Error while fetching basic details', error);
    }
  );
}

subscribeToStudentDetails() {
  this.studentDetailsSubscription = this.saveCaseService.subjectStudentDetails.subscribe(
    next => {
      this.studentDetails = next;
    },
    error => {
      console.error('Error while fetching student details', error);
    }
  );
}

subscribeToCoApplicantDetails() {
  this.coApplicantDetailsSubscription = this.saveCaseService.subjectCoApplicantDetails.subscribe(
    next => {
      this.coApplicant = next;

    },
    error => {
      console.error('Error while fetching student details', error);
    }
  );
}

ngOnInit() {
  this.headerService.setTitle('Co-Applicant Details');
  this.subscribeToBasicDetails();
  this.subscribeToStudentDetails();
  this.subscribeToCoApplicantDetails();
  this.saveCaseService.getBasicDetails();
  this.saveCaseService.getStudentDetails();
  this.saveCaseService.getCoApplicantDetails();
  this.subscribeToSaveCase();
}

backClicked() {
  this._location.back();
}

handleFileSelect(evt, type) {
  let files = evt.target.files;
  const file = files[0];
  this.fileType = type;
  this.fileName = file.name;
  console.log(file);
  if (files && file) {
    const reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(file);
  }
}

Printparent1(event: any) {
  this.parmanent_address = this.current_address;
} 

_handleReaderLoaded(readerEvt) {
  const binaryString = readerEvt.target.result;
  this.base64textString = btoa(binaryString);

  if (this.fileType === 'aadhar') {
    const document = new Document();
    document.content = this.base64textString;
    document.belongsTo = 'co-applicant';
    document.type = 'Aadhar';
    document.fileName = this.fileName;
    this.oneStepService.uploadDocument(document).subscribe(docRes => {
      if(docRes.success){
        this.coApplicant.aadharReceived = true;
        this.saveCaseService.setcoApplicantDetails(this.coApplicant);
      }
    });
  }
  if (this.fileType === 'pancard') {
    const document = new Document();
    document.content = this.base64textString;
    document.belongsTo = 'co-applicant';
    document.type = 'PAN';
    document.fileName = this.fileName;
    this.oneStepService.uploadDocument(document).subscribe(docRes => {
      if(docRes.success){
        this.coApplicant.panReceived = true;
        this.saveCaseService.setcoApplicantDetails(this.coApplicant);
      }
    });
  }
  if (this.fileType === 'voterid') {
    const document = new Document();
    document.content = this.base64textString;
    document.belongsTo = 'co-applicant';
    document.type = 'voterId';
    document.fileName = this.fileName;
    this.oneStepService.uploadDocument(document).subscribe(docRes => {
      if(docRes.success){
        this.coApplicant.voterIdReceived = true;
        this.saveCaseService.setcoApplicantDetails(this.coApplicant);
      }
    });
  }
}

}
