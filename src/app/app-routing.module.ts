import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { MobileRegistrationComponent } from './mobile-registration/mobile-registration.component';
import { VerificationComponent } from './verification/verification.component';
import { EmailComponent } from './email/email.component';
import { PancardComponent } from './pancard/pancard.component';
import { LoanPurposeComponent } from './loan-purpose/loan-purpose.component';
import { NewloanComponent } from './newloan/newloan.component';
import { StudentDetailsComponent } from './student-details/student-details.component';
import { CoApplicantComponent } from './co-applicant/co-applicant.component';
import { ParentComponent } from './parent/parent.component';
import { CourseComponent } from './course/course.component';
import { OneStepAwayComponent } from './one-step-away/one-step-away.component';
import { ScanComponent } from './scan/scan.component';
import { ScanPassportComponent } from './scan-passport/scan-passport.component';
import { ScanPancardComponent } from './scan-pancard/scan-pancard.component';
import { ScanAadhaarcardComponent } from './scan-aadhaarcard/scan-aadhaarcard.component';
import { CoApplicantNfComponent } from './co-applicant-nf/co-applicant-nf.component';
import { VerifyPassportComponent } from './verify-passport/verify-passport.component';
import { PerfiosComponent } from './perfios/perfios.component';
import { VerifyPancardComponent } from './verify-pancard/verify-pancard.component';
import { VerifyAadhaarcardComponent } from './verify-aadhaarcard/verify-aadhaarcard.component';
import { summaryFileName } from '@angular/compiler/src/aot/util';
import { PerfiosBankComponent } from './perfiosbank/perfiosbank.component';
import { SummaryComponent } from './summary/summary.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { DocumentUploadComponent } from './document-upload/document-upload.component';
import { AntiAuthGuard, AuthGuard } from './services/auth-guard.service';
import { TandcComponent } from './tandc/tandc.component';
import { StartPerfiosComponent } from './startperfios/startperfios.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { CommonVerificationComponent } from './common-verification/common-verification.component';

const routes: Routes = [
  {path: '', component: GetstartedComponent},
  {path: 'mobile-registration', component: MobileRegistrationComponent},
  {path: 'mobile-login', component: MobileRegistrationComponent},
  {path: 'verification/:otpSource', component: VerificationComponent, canActivate: [AntiAuthGuard]},
  {path: 'email', component: EmailComponent, canActivate: [AntiAuthGuard]},

  {path: 'pancard', component: PancardComponent, canActivate: [AuthGuard]},
  {path: 'loan-purpose', component: LoanPurposeComponent, canActivate: [AuthGuard]},
  {path: 'newloan', component: NewloanComponent, canActivate: [AuthGuard]},
  {path: 'student-details', component: StudentDetailsComponent, canActivate: [AuthGuard]},
  {path: 'co-applicant', component: CoApplicantComponent, canActivate: [AuthGuard]},
  {path: 'parent', component: ParentComponent, canActivate: [AuthGuard]},
  {path: 'course', component: CourseComponent, canActivate: [AuthGuard]},
  {path: 'one-step-away', component: OneStepAwayComponent, canActivate: [AuthGuard]},
  {path: 'scan', component: ScanComponent, canActivate: [AuthGuard]},
  {path: 'scan-passport', component: ScanPassportComponent, canActivate: [AuthGuard]},
  {path: 'scan-pancard', component: ScanPancardComponent, canActivate: [AuthGuard]},
  {path: 'scan-aadhaarcard', component: ScanAadhaarcardComponent, canActivate: [AuthGuard]},
  {path: 'co-applicant-nf', component: CoApplicantNfComponent, canActivate: [AuthGuard]},
  {path: 'verify-passport', component: VerifyPassportComponent, canActivate: [AuthGuard]},
  {path: 'verify-pancard', component: VerifyPancardComponent, canActivate: [AuthGuard]},
  {path: 'verify-aadhaarcard', component: VerifyAadhaarcardComponent, canActivate: [AuthGuard]},
  {path: 'summary', component: SummaryComponent, canActivate: [AuthGuard]},
  {path: 'thankyou', component: ThankyouComponent, canActivate: [AuthGuard]},
  {path: 'document-upload', component: DocumentUploadComponent, canActivate: [AuthGuard]},
  {path: 'perfios', component: PerfiosComponent, canActivate: [AuthGuard]},
  {path: 'perfiosstatementcompleted', component: PerfiosBankComponent, canActivate: [AuthGuard]},
  {path: 'tandc', component: TandcComponent},

  {path: 'startperfios', component: StartPerfiosComponent, canActivate: [AuthGuard]},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'common-verification', component: CommonVerificationComponent},
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
