import { Component } from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StudentDetailsComponent } from './student-details/student-details.component';
import { CoApplicantComponent } from './co-applicant/co-applicant.component';
import { ParentComponent } from './parent/parent.component';
import { HeaderService } from './header.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  
})
  

export class AppComponent {
  constructor(
    private modalService: NgbModal, private headerService: HeaderService
  ) { }
  title = 'auxilo-self';
  status: any = 0;  
  // state: string ='small'
  // animateMe(){
  //   this.state = (this.state === 'small' ? 'large' : 'small');
  // }
  openFormModal() {
    const modalRef = this.modalService.open([StudentDetailsComponent,CoApplicantComponent,ParentComponent]);
    
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.headerService.title.subscribe(title => {
      this.title = title;
    });
    // this.headerService.status.subscribe(status => {
    //   this.status = status;
    // });
  }
}
