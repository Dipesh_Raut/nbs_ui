import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HeaderService } from '../header.service';
import { Location } from '@angular/common';
import { Parent } from './shared/parent.model';
import { Router } from '@angular/router';
import { SaveCaseService } from '../services/saveCase.service';
import { Subscription } from 'rxjs';
import { CoApplicant } from '../co-applicant/shared/co-applicant.model';
import { Document } from '../model/document.model';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  /* first_name1 = '';
  middle_name1 = '';
  last_name1 = '';
  gender1 = '';
  mobile_number1 = '';
  email_id1 = '';
  current_address1 = '';
  parmanent_address1 = '';
  
  first_name2 = '';
  middle_name2 = '';
  last_name2 = '';
  gender2 = '';
  mobile_number2 = '';
  email_id2 = '';
  current_address2 = '';
  parmanent_address2 = '';
  saca2 = false; */
  saca1 = false;
  saca2 = false;
  parent: Parent = new Parent();
  coApplicant: CoApplicant = new CoApplicant();
  closeResult = '';

  parentDetailsSubscription: Subscription;
  saveCaseSubscription: Subscription;
  coApplicantDetailsSubscription: Subscription;

  fileType: string;
  fileName: string;
  base64textString: string;

  fatherAadharCardMessage: string = 'Please upload Aadhar Card';
  fatherPanCardMessage: string = 'Please upload PAN Card';
  fatherVoterIdMessage: string = 'Please upload Voter ID';
  motherAadharCardMessage: string = 'Please upload Aadhar Card';
  motherPanCardMessage: string = 'Please upload PAN Card';
  motherVoterIdMessage: string = 'Please upload Voter ID';

  fatherAadharDisable: boolean;
  fatherPanDisable: boolean;
  fatherVoterDisable: boolean;
  motherAadharDisable: boolean;
  motherPanDisable: boolean;
  motherVoterDisable: boolean;

  constructor(
    private modalService: NgbModal,
    private headerService: HeaderService,
    private _location: Location,
    private router: Router,
    private saveCaseService: SaveCaseService
  ) { }

  parentProcess() {
    console.log(this.parent);
    this.saveCaseService.setParentDetails(this.parent);
    
  }

  handleFileSelect(event,type){
    if(type === "fatherAadhar"){
      this.fatherAadharDisable = true;
    }
    if(type === "motherAadhar"){
      this.motherAadharDisable = true;
    }
    if(type === "fatherPan"){
      this.fatherPanDisable = true;
    }
    if(type === "motherPan"){
      this.motherPanDisable = true;
    }
    if(type === "fatherVoter"){
      this.fatherVoterDisable = true;
    }
    if(type === "motherVoter"){
      this.motherVoterDisable = true;
    }

  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.headerService.setTitle('Parent Details');

    this.subscribeToParentDetails();
    this.subscribeToCoApplicantDetails();
    this.subscribeToSaveCase();
    this.saveCaseService.getParentDetails();
  }

  ngOnDestroy() {
    if (this.parentDetailsSubscription && !this.parentDetailsSubscription.closed) {
      this.parentDetailsSubscription.unsubscribe();
    }
    if (this.saveCaseSubscription && !this.saveCaseSubscription.closed) {
      this.saveCaseSubscription.unsubscribe();
    }
    if (this.coApplicantDetailsSubscription && !this.coApplicantDetailsSubscription.closed) {
      this.coApplicantDetailsSubscription.unsubscribe();
    }
  }

  subscribeToParentDetails() {
    this.parentDetailsSubscription = this.saveCaseService.subjectParentDetails.subscribe(
      next => {
        this.parent = next;

        if (!this.parent.father_documents) {
          this.parent.father_documents = [];
        }

        if (!this.parent.mother_documents) {
          this.parent.mother_documents = [];
        }

        if (this.parent.father_documents) {
          for (const doc of this.parent.father_documents) {
            if (doc && doc.type && doc.type.trim().toLowerCase() == 'aadhar') {
              this.fatherAadharCardMessage = doc.fileName;
              this.fatherAadharDisable = true;
            } else {
              this.fatherAadharCardMessage = 'Please upload Aadhar Card';
              this.fatherAadharDisable = false;
            }

            if (doc && doc.type && doc.type.trim().toLowerCase() == 'pan') {
              this.fatherPanCardMessage = doc.fileName;
              this.fatherPanDisable = true;
            } else {
              this.fatherPanCardMessage = 'Please upload PAN Card';
              this.fatherPanDisable = false;
            }

            if (doc && doc.type && doc.type.trim().toLowerCase() == 'voter') {
              this.fatherVoterIdMessage = doc.fileName;
              this.fatherVoterDisable = true;
            } else {
              this.fatherVoterIdMessage = 'Please upload Voter ID';
              this.fatherVoterDisable = false;
            }
          }
        }

        if (this.parent.mother_documents) {
          for (const doc of this.parent.mother_documents) {
            if (doc && doc.type && doc.type.trim().toLowerCase() == 'aadhar') {
              this.motherAadharCardMessage = doc.fileName;
              this.motherAadharDisable = true;
            } else {
              this.motherAadharCardMessage = 'Please upload Aadhar Card';
              this.motherAadharDisable = false;
            }

            if (doc && doc.type && doc.type.trim().toLowerCase() == 'pan') {
              this.motherPanCardMessage = doc.fileName;
              this.motherPanDisable = true;
            } else {
              this.motherPanCardMessage = 'Please upload PAN Card';
              this.motherPanDisable = false;
            }

            if (doc && doc.type && doc.type.trim().toLowerCase() == 'voter') {
              this.motherVoterIdMessage = doc.fileName;
              this.motherVoterDisable = true;
            } else {
              this.motherVoterIdMessage = 'Please upload Voter ID';
              this.motherVoterDisable = false;
            }
          }
        }
      },
      error => {

      }
    );
  }

  subscribeToCoApplicantDetails() {
    this.coApplicantDetailsSubscription = this.saveCaseService.subjectCoApplicantDetails.subscribe(
      next => {
        this.coApplicant = next;
      },
      error => {

      }
    );
  }

  subscribeToSaveCase() {
    this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
      next => {
        this.router.navigateByUrl("/summary");
      },
      error => {

      }
    );
  }

  backClicked() {
    this._location.back();
  }
  //TO DO: check father mother address error if any
/*   Print(event: any) {
    this.parent.father_permenantAdd = this.parent.father_currentAdd;
  }
  Printparent1(event: any) {
    this.parent.mother_permenantAdd = this.parent.mother_currentAdd;
  } */

  // handleFileSelect(evt, type) {
  //   let files = evt.target.files;
  //   const file = files[0];
  //   this.fileType = type
  //   this.fileName = file.name;
  //   console.log(file);
  //   if (files && file) {
  //     const reader = new FileReader();
  //     reader.onload = this._handleReaderLoaded.bind(this);
  //     reader.readAsBinaryString(file);
  //   }
  // }

  // _handleReaderLoaded(readerEvt) {
  //   const binaryString = readerEvt.target.result;
  //   this.base64textString = btoa(binaryString);

  //   if (this.fileType === 'father aadhar') {
  //     this.fatherAadharCardMessage = this.fileName;
  //     const fatherAadhar = new Document();
  //     fatherAadhar.content = this.base64textString;
  //     fatherAadhar.belongsTo = 'applicant';
  //     fatherAadhar.type = 'Aadhar';
  //     fatherAadhar.fileName = this.fileName;

  //     this.removeOldDocument(fatherAadhar.type, this.parent.father_documents);
  //     this.parent.father_documents.push(fatherAadhar);
  //   }
  //   if (this.fileType === 'father pan') {
  //     this.fatherPanCardMessage = this.fileName;
  //     const fatherPan = new Document();
  //     fatherPan.content = this.base64textString;
  //     fatherPan.belongsTo = 'applicant';
  //     fatherPan.type = 'PAN';
  //     fatherPan.fileName = this.fileName;

  //     this.removeOldDocument(fatherPan.type, this.parent.father_documents);
  //     this.parent.father_documents.push(fatherPan);
  //   }
  //   if (this.fileType === 'father voter') {
  //     this.fatherVoterIdMessage = this.fileName;
  //     const fatherVoter = new Document();
  //     fatherVoter.content = this.base64textString;
  //     fatherVoter.belongsTo = 'applicant';
  //     fatherVoter.type = 'Voter';
  //     fatherVoter.fileName = this.fileName;

  //     this.removeOldDocument(fatherVoter.type, this.parent.father_documents);
  //     this.parent.father_documents.push(fatherVoter);
  //   }
  //   if (this.fileType === 'mother aadhar') {
  //     this.motherAadharCardMessage = this.fileName;
  //     const motherAadhar = new Document();
  //     motherAadhar.content = this.base64textString;
  //     motherAadhar.belongsTo = 'applicant';
  //     motherAadhar.type = 'Aadhar';
  //     motherAadhar.fileName = this.fileName;

  //     this.removeOldDocument(motherAadhar.type, this.parent.father_documents);
  //     this.parent.mother_documents.push(motherAadhar);
  //   }
  //   if (this.fileType === 'mother pan') {
  //     this.motherPanCardMessage = this.fileName;
  //     const motherPan = new Document();
  //     motherPan.content = this.base64textString;
  //     motherPan.belongsTo = 'applicant';
  //     motherPan.type = 'PAN';
  //     motherPan.fileName = this.fileName;

  //     this.removeOldDocument(motherPan.type, this.parent.father_documents);
  //     this.parent.mother_documents.push(motherPan);
  //   }
  //   if (this.fileType === 'mother voter') {
  //     this.motherVoterIdMessage = this.fileName;
  //     const motherVoter = new Document();
  //     motherVoter.content = this.base64textString;
  //     motherVoter.belongsTo = 'applicant';
  //     motherVoter.type = 'Voter';
  //     motherVoter.fileName = this.fileName;

  //     this.removeOldDocument(motherVoter.type, this.parent.father_documents);
  //     this.parent.mother_documents.push(motherVoter);
  //   }
  // }

  // removeOldDocument(type: string, source: Document[]) {
  //   let oldIndex = -1;
  //   if (type && source) {
  //     for (let doc of source) {
  //       if (doc && doc.type && doc.type.trim().toLocaleLowerCase() == type.trim().toLocaleLowerCase()) {
  //         oldIndex = source.indexOf(doc);
  //         break;
  //       }
  //     }
  //     if (oldIndex > -1 && oldIndex < source.length) {
  //       source.splice(oldIndex, 1);
  //     }
  //   }
  // }

}
