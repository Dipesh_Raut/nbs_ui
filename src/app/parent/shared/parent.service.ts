import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Parent } from './parent.model';

@Injectable()
export class ParentService {

	constructor(private http: Http) { }

	getList(): Observable<Parent[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Parent[]));
	}
	postOrder(cart): Observable<Parent[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Parent[]));
	}
}