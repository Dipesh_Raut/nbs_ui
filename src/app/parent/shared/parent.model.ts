import { Document } from '../../model/document.model';

export class Parent {
	father_first_name : string = '';
	father_middle_name : string = '';
	father_last_name : string = '';
	father_emailId: string = '';
	father_mobile: string = '';
	father_currentAdd : string = '';
	father_permenantAdd : string = '';
	father_documents :Document[] = [];
	father_sameAsCurrentAdd:boolean = false;
	
	mother_first_name : string = '';
	mother_middle_name : string = '';
	mother_last_name : string = '';
	mother_emailId: string = '';
	mother_mobile: string = '';
	mother_currentAdd : string = '';
	mother_permenantAdd : string = '';
	mother_documents :Document[] = [];
	mother_sameAsCurrentAdd:boolean = false;
}