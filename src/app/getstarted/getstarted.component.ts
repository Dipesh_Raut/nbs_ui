import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-getstarted',
  templateUrl: './getstarted.component.html',
  styleUrls: ['./getstarted.component.css']
})
export class GetstartedComponent implements OnInit {
  tandc = false;
  constructor(
    private headerService: HeaderService
  ) { }

  ngOnInit() {
    this.headerService.setTitle('');
  }
  
}
