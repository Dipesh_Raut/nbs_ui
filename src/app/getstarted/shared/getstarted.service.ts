import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Getstarted } from './getstarted.model';

@Injectable()
export class GetstartedService {

	constructor(private http: Http) { }

	getList(): Observable<Getstarted[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Getstarted[]));
	}
	postOrder(cart): Observable<Getstarted[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Getstarted[]));
	}
}