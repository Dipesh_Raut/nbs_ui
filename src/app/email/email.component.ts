import { Component, OnInit } from '@angular/core';
import { VerificationService } from '../services/verification.service';
import { Router } from '@angular/router';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  success = '';
  countryflag = '';
  successicon = '';
  email: string = '';

  constructor(private router: Router,
    private verificationService: VerificationService,
    private headerService: HeaderService) { }

    public processEmail() {
      if(this.email){
        
      this.countryflag = 'assets/images/india-flag.png';
      this.success = 'success'; // danger for error
      this.successicon = 'fa-check-circle'; // fa-times-circle
      }
      
    }

  gotoOtp() { 
    console.log(this.email);
    this.verificationService.getEmailOtp(this.email).subscribe(response => {
      if(response.success) {
        this.router.navigateByUrl('verification/email');
      }
      else{
        //TO DO: error handeling
      }
    })
  }
  ngOnInit() {
    this.headerService.setTitle('');
  }

}
