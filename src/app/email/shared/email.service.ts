import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../shared/config';
import { Email } from './email.model';

@Injectable()
export class EmailService {

	constructor(private http: Http) { }

	getList(): Observable<Email[]> {
		return this.http.get('/api/list').pipe(map(res => res.json() as Email[]));
	}
	postOrder(cart): Observable<Email[]> {
		return this.http.post( Config.apiURL + '/cart', cart).pipe(map(res => res.json() as Email[]));
	}
}