import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-scan-pancard',
  templateUrl: './scan-pancard.component.html',
  styleUrls: ['./scan-pancard.component.css']
})
export class ScanPancardComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Scan Pancard');
  }

}
