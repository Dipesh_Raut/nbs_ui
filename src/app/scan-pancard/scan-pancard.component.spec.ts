import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanPancardComponent } from './scan-pancard.component';

describe('ScanPancardComponent', () => {
  let component: ScanPancardComponent;
  let fixture: ComponentFixture<ScanPancardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanPancardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanPancardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
