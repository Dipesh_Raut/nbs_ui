import { Component, OnInit,OnDestroy } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HeaderService } from '../header.service';
import { BasicDetails } from '../model/basicDetails.model';
import { Parent } from '../parent/shared/parent.model';
import { SaveCaseService } from '../services/saveCase.service';
import { StudentDetails } from '../student-details/shared/student-details.model';
import { CoApplicant } from '../co-applicant/shared/co-applicant.model';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from '../services/session.service';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-perfiosbank',
  templateUrl: './perfiosbank.component.html',
  styleUrls: ['./perfiosbank.component.css']
})
export class PerfiosBankComponent implements OnInit, OnDestroy {
    perfiosTransactionCompleted: string;
    perfiosTransactionFailure: string;
    perfiosTransactionTimeout: string;
    perfiosTransactionCancelled: string;
    perfiosTransactionError: string;
    attributes = {};
    studentDetails: StudentDetails;
    coApplicantDetails: CoApplicant;
    message: string;
    buttonMessage: string;
    studentDetailsSubscription: Subscription;
    saveCaseSubscription: Subscription;
    coAppDetailsSubscription: Subscription;

    //private subscription: Subscription;
    constructor(
        private route: ActivatedRoute,
        private saveCaseService: SaveCaseService,
        private sessionService: SessionService
    ) {
        this.perfiosTransactionCompleted = 'COMPLETED';
        this.perfiosTransactionCancelled = 'CANCELLED';
        this.perfiosTransactionError = 'ERROR';
        this.perfiosTransactionFailure = 'REPORT_DELIVERY_FAILED';
        this.perfiosTransactionTimeout = 'TIMEOUT';
        this.message = '';
        this.buttonMessage = '';
        this.studentDetails = new StudentDetails();
        this.coApplicantDetails = new CoApplicant();

    }
    ngOnInit() {
        if(this.sessionService.getApplicantType() === "student"){
            this.subscribeToStudentDetails();
        }
        if(this.sessionService.getApplicantType() === "coapplicant"){
            this.subscribeToCoApplicantDetails();
        }
        this.route.params.subscribe(params => {
            this.attributes = this.route.snapshot.queryParams;
            console.log(this.attributes);
        });
        this.saveCaseService.getStudentDetails();
        this.saveCaseService.getCoApplicantDetails();
        this.subscribeToSaveCase();
    }

    ngOnDestroy() {
        if (this.studentDetailsSubscription && !this.studentDetailsSubscription.closed) {
            this.studentDetailsSubscription.unsubscribe();
        }
        if (this.coAppDetailsSubscription && !this.coAppDetailsSubscription.closed) {
            this.coAppDetailsSubscription.unsubscribe();
        }
    }

    proceed() {
        window.close();
    }


    subscribeToSaveCase() {
        this.saveCaseSubscription = this.saveCaseService.subjectSaveCase.subscribe(
          next => {
          },
          error => {
          }
        );
      }
    subscribeToStudentDetails() {
        this.studentDetailsSubscription = this.saveCaseService.subjectStudentDetails.subscribe(
            next => {
                this.studentDetails = next;
                this.studentDetails.bankStatementReceived = true;
                this.message = "Your Bank Statement was fetched successfully";
                this.buttonMessage = 'Continue Application';
                this.saveCaseService.setStudentDetails(this.studentDetails);
            },
            error => {
                console.error('Error while fetching student details', error);
            }
        );
    }

    subscribeToCoApplicantDetails() {
        this.coAppDetailsSubscription = this.saveCaseService.subjectCoApplicantDetails.subscribe(
            next => {
                this.coApplicantDetails = next;
                this.coApplicantDetails.bankStatementReceived = true;
                this.message = "Your Bank Statement was fetched successfully";
                this.buttonMessage = 'Continue Application';
                this.saveCaseService.setcoApplicantDetails(this.coApplicantDetails);
            },
            error => {
                console.error('Error while fetching student details', error);
            }
        );
    }
}
