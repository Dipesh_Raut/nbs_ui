import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header.service';

@Component({
  selector: 'app-scan-passport',
  templateUrl: './scan-passport.component.html',
  styleUrls: ['./scan-passport.component.css']
})
export class ScanPassportComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Scan Passport');
  }

}
