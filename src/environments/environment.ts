// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  baseUrl: "https://api.automatapi.com",
  smsOtpUrl : "/generateAndSendGupshupSmsOtp",
  emailOtpUrl: "/generateAndSendEmailOtp",
  authenticationUrl : "https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/authenticate",

  authtoken :"Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1YjY5MzYzYTc4NGI3ZjRmZjNiNTI2OGQiLCJzdWIiOiJBdXhpbG8iLCJpc3MiOiJrY1FtUnkzMUNOMGhBaXU5bHFYQmRqdHNaaHlxN0ZPVCJ9.QbZmNqIMRDdWyW32Bi7y1t6P1r9KGx95kPhGWzQUSoo",
  username : "auxilo@automatapi.com",
  password : "admin",
  companyId : "5b69363a784b7f4ff3b5268d",
  companyName :"Auxilo",
  googlevision: "/cloudGoogleVisionToExtractData",
  perfiostransaction:"https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/perfios/startfetchitr",
  fetchitr:'https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/perfios/fetchitr',
  saveCaseUrl: "https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/savecase",
  fetchCaseUrl: "https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/fetchcase",
  caseUrl: "https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/case",
  perfiosbank:'https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/perfios/startbankanalysis',
  validatepan:"https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/validatepan",
  lookupUrl: "https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/lookup",
  uploaddocument:"https://hg8hxpjrg2.execute-api.ap-south-1.amazonaws.com/test/auxilo/document",
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
