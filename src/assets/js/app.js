closeModal = function (modalId) {
    if ($('#' + modalId).is(':visible')) {
      $('#' + modalId).modal('hide');
    }
  }
  
  showModal = function (modalId) {
    if (!$('#' + modalId).is(':visible')) {
      $('#' + modalId).modal();
    }
  }