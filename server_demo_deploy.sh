# Root of server
ng build --configuration=development

ssh ubuntu@35.154.117.220 "rm -rf auxiloportal_temp"
ssh ubuntu@35.154.117.220 "mkdir auxiloportal_temp"
scp -r ./dist/auxilo-self/* ubuntu@35.154.117.220:~/auxiloportal_temp/
ssh ubuntu@35.154.117.220 "sudo rm -rf /var/www/auxiloportal/*"
ssh ubuntu@35.154.117.220 "sudo cp -r ./auxiloportal_temp/* /var/www/auxiloportal/"
ssh ubuntu@35.154.117.220 "rm -rf auxiloportal_temp"

# sudo rm -rf /var/www/html/*
# sudo cp -r ./dist/auxilo-self/* /var/www/html/